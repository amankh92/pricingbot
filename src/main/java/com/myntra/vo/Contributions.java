package com.myntra.vo;

public class Contributions {
	int aviator;
	int health;
	int mc;
	public int getAviator() {
		return aviator;
	}
	public void setAviator(int aviator) {
		this.aviator = aviator;
	}
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public int getMc() {
		return mc;
	}
	public void setMc(int mc) {
		this.mc = mc;
	}
	
}
