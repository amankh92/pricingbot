package com.myntra.vo;

public class StyleDetails {

	String styleid;
	String getpercent;
	String last30dayssales;
	String last7dayssales;
	double dmdfinal;
	String articlemrp;
	String articletype;
	String gender;
	String discountfunding;
	String fundingpercentage;
	String discountlimit;
	String from_unixtime_expired_on;
	String getcount;
	String getamount;
	String brand;
	String discountid;
	double newbuyingmargin;
	double norm_adj_bm;
	double currentTd;
	double newTd;
	double maxDiscount;
	double minDiscount;
	//double dmdFinal;
	double totalscore;
	double netscore;
	int elasticityBand;
	String brandtype; 
	String currentcommercialtype;
	String businessunit;
	String cascElasticity_unif = "NA";
	String cascElasticity_TD = "NA";
	String cascElasticity_CD = "NA";
	String cascVisibility_Elasticity = "NA";
	String averageage;
	String seasoncode;
	double norm_adj_bm_new;
	double norm_adj_bm_new_actual;
	
	double newDemand;
	double rankingFactor;
	
	
	int norm_adj_bm_breach;
	int health;
	
	public String getStyleid() {
		return styleid;
	}
	public void setStyleid(String styleid) {
		this.styleid = styleid;
	}
	public String getGetpercent() {
		return getpercent;
	}
	public void setGetpercent(String getpercent) {
		this.getpercent = getpercent;
	}
	public String getLast30dayssales() {
		return last30dayssales;
	}
	public void setLast30dayssales(String last30dayssales) {
		this.last30dayssales = last30dayssales;
	}
	public String getLast7dayssales() {
		return last7dayssales;
	}
	public void setLast7dayssales(String last7dayssales) {
		this.last7dayssales = last7dayssales;
	}
	public double getDmdfinal() {
		return dmdfinal;
	}
	public void setDmdfinal(double dmdfinal) {
		this.dmdfinal = dmdfinal;
	}
	public String getArticlemrp() {
		return articlemrp;
	}
	public void setArticlemrp(String articlemrp) {
		this.articlemrp = articlemrp;
	}
	public String getArticletype() {
		return articletype;
	}
	public void setArticletype(String articletype) {
		this.articletype = articletype;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDiscountfunding() {
		return discountfunding;
	}
	public void setDiscountfunding(String discountfunding) {
		this.discountfunding = discountfunding;
	}
	public String getFundingpercentage() {
		return fundingpercentage;
	}
	public void setFundingpercentage(String fundingpercentage) {
		this.fundingpercentage = fundingpercentage;
	}
	public String getDiscountlimit() {
		return discountlimit;
	}
	public void setDiscountlimit(String discountlimit) {
		this.discountlimit = discountlimit;
	}
	public String getFrom_unixtime_expired_on() {
		return from_unixtime_expired_on;
	}
	public void setFrom_unixtime_expired_on(String from_unixtime_expired_on) {
		this.from_unixtime_expired_on = from_unixtime_expired_on;
	}
	public String getGetcount() {
		return getcount;
	}
	public void setGetcount(String getcount) {
		this.getcount = getcount;
	}
	public String getGetamount() {
		return getamount;
	}
	public void setGetamount(String getamount) {
		this.getamount = getamount;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getDiscountid() {
		return discountid;
	}
	public void setDiscountid(String discountid) {
		this.discountid = discountid;
	}
	public double getCurrentTd() {
		return currentTd;
	}
	public void setCurrentTd(double currentTd) {
		this.currentTd = currentTd;
	}
	public double getNewTd() {
		return newTd;
	}
	public void setNewTd(double newTd) {
		this.newTd = newTd;
	}
	public double getMaxDiscount() {
		return maxDiscount;
	}
	public void setMaxDiscount(double maxDiscount) {
		this.maxDiscount = maxDiscount;
	}
	public double getMinDiscount() {
		return minDiscount;
	}
	public void setMinDiscount(double minDiscount) {
		this.minDiscount = minDiscount;
	}
	/*public double getDmdFinal() {
		return dmdFinal;
	}
	public void setDmdFinal(double dmdFinal) {
		this.dmdFinal = dmdFinal;
	}*/
	public double getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(double totalscore) {
		this.totalscore = totalscore;
	}
	public int getElasticityBand() {
		return elasticityBand;
	}
	public void setElasticityBand(int elasticityBand) {
		this.elasticityBand = elasticityBand;
	}
	public int getHealth() {
		return health;
	}
	public void setHealth(int health) {
		this.health = health;
	}
	public double getNewbuyingmargin() {
		return newbuyingmargin;
	}
	public void setNewbuyingmargin(double newbuyingmargin) {
		this.newbuyingmargin = newbuyingmargin;
	}
	
	public double getNorm_adj_bm() {
		return norm_adj_bm;
	}
	public void setNorm_adj_bm(double norm_adj_bm) {
		this.norm_adj_bm = norm_adj_bm;
	}
	public String getBrandtype() {
		return brandtype;
	}
	public void setBrandtype(String brandtype) {
		this.brandtype = brandtype;
	}
	public String getCurrentcommercialtype() {
		return currentcommercialtype;
	}
	public void setCurrentcommercialtype(String currentcommercialtype) {
		this.currentcommercialtype = currentcommercialtype;
	}
	public String getBusinessunit() {
		return businessunit;
	}
	public void setBusinessunit(String businessunit) {
		this.businessunit = businessunit;
	}
	public String getCascElasticity_unif() {
		return cascElasticity_unif;
	}
	public void setCascElasticity_unif(String cascElasticity_unif) {
		this.cascElasticity_unif = cascElasticity_unif;
	}
	public String getCascElasticity_TD() {
		return cascElasticity_TD;
	}
	public void setCascElasticity_TD(String cascElasticity_TD) {
		this.cascElasticity_TD = cascElasticity_TD;
	}
	public String getCascElasticity_CD() {
		return cascElasticity_CD;
	}
	public void setCascElasticity_CD(String cascElasticity_CD) {
		this.cascElasticity_CD = cascElasticity_CD;
	}
	public String getCascVisibility_Elasticity() {
		return cascVisibility_Elasticity;
	}
	public void setCascVisibility_Elasticity(String cascVisibility_Elasticity) {
		this.cascVisibility_Elasticity = cascVisibility_Elasticity;
	}
	public int getNorm_adj_bm_breach() {
		return norm_adj_bm_breach;
	}
	public void setNorm_adj_bm_breach(int norm_adj_bm_breach) {
		this.norm_adj_bm_breach = norm_adj_bm_breach;
	}
	public double getNorm_adj_bm_new() {
		return norm_adj_bm_new;
	}
	public void setNorm_adj_bm_new(double norm_adj_bm_new) {
		this.norm_adj_bm_new = norm_adj_bm_new;
	}
	public double getNorm_adj_bm_new_actual() {
		return norm_adj_bm_new_actual;
	}
	public void setNorm_adj_bm_new_actual(double norm_adj_bm_new_actual) {
		this.norm_adj_bm_new_actual = norm_adj_bm_new_actual;
	}
	public String getAverageage() {
		return averageage;
	}
	public void setAverageage(String averageage) {
		this.averageage = averageage;
	}
	public double getNetscore() {
		return netscore;
	}
	public void setNetscore(double netscore) {
		this.netscore = netscore;
	}
	public double getRankingFactor() {
		return rankingFactor;
	}
	public void setRankingFactor(double rankingFactor) {
		this.rankingFactor = rankingFactor;
	}
	public double getNewDemand() {
		return newDemand;
	}
	public void setNewDemand(double newDemand) {
		this.newDemand = newDemand;
	}
	public String getSeasoncode() {
		return seasoncode;
	}
	public void setSeasoncode(String seasoncode) {
		this.seasoncode = seasoncode;
	}
	
}
