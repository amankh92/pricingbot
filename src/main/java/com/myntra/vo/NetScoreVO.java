package com.myntra.vo;

public class NetScoreVO {
	String styleId;
	double netScore;
	
	public String getStyleId() {
		return styleId;
	}
	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}
	public double getNetScore() {
		return netScore;
	}
	public void setNetScore(double netScore) {
		this.netScore = netScore;
	}
	
}
