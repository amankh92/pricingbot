package com.myntra.vo;

/**
 * Created by 11138 on 08/03/17.
 */
public class Category {
    public String getArticle_type() {
        return article_type;
    }

    public void setArticle_type(String article_type) {
        this.article_type = article_type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    private String article_type;
    private String gender;

    public Category(String article_type, String gender){
        this.article_type = article_type;
        this.gender = gender;
    }
}
