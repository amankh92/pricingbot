package com.myntra.vo;

public class StyleDemand {

    private String style_id;
    private double demand;
    private double cascVisibility_Elasticity;
    private double visibilityDemand;

    public String getStyle_id() {
        return style_id;
    }
    public void setStyle_id(String style_id) {
        this.style_id = style_id;
    }
    public double getDemand() {
        return demand;
    }
    public void setDemand(double demand) {
        this.demand = demand;
    }
    public double getCascVisibility_Elasticity() {
        return cascVisibility_Elasticity;
    }
    public void setCascVisibility_Elasticity(double cascVisibility_Elasticity) {
        this.cascVisibility_Elasticity = cascVisibility_Elasticity;
    }
    public double getVisibilityDemand() {
        return visibilityDemand;
    }
    public void setVisibilityDemand(double visibilityDemand) {
        this.visibilityDemand = visibilityDemand;
    }
}
