package com.myntra.vo;

public class ListViews {

	private int date = 0;
	private String sku_id = null;
	private String style_id = null;
	private String commercial_type = null;
	private String article_type = null;
	private String brand_type = null;
	private String brand = null;
	private String gender = null;
	private long list = 0;
	private long pdp = 0;
	private long cart = 0;
	private long styleQtySold = 0;
	private long skuQtySold = 0;
	private int islive = 0;


	public String getStyle_id() {
		return style_id;
	}
	public void setStyle_id(String style_id) {
		this.style_id = style_id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public String getCommercial_type() {
		return commercial_type;
	}
	public void setCommercial_type(String commercial_type) {
		this.commercial_type = commercial_type;
	}
	public String getArticle_type() {
		return article_type;
	}
	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}
	public String getBrand_type() {
		return brand_type;
	}
	public void setBrand_type(String brand_type) {
		this.brand_type = brand_type;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getList() {
		return list;
	}
	public void setList(long list) {
		this.list = list;
	}
	public long getPdp() {
		return pdp;
	}
	public void setPdp(long pdp) {
		this.pdp = pdp;
	}
	public long getCart() {
		return cart;
	}
	public void setCart(long cart) {
		this.cart = cart;
	}
	public String getSku_id() {
		return sku_id;
	}
	public void setSku_id(String sku_id) {
		this.sku_id = sku_id;
	}
	public long getStyleQtySold() {
		return styleQtySold;
	}
	public void setStyleQtySold(long styleQtySold) {
		this.styleQtySold = styleQtySold;
	}
	public long getSkuQtySold() {
		return skuQtySold;
	}
	public void setSkuQtySold(long skuQtySold) {
		this.skuQtySold = skuQtySold;
	}
	public int getIslive() {
		return islive;
	}
	public void setIslive(int islive) {
		this.islive = islive;
	}

}
