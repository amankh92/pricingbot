package com.myntra.vo;

public class CategoryStyleRow {

	String category;
	String style_id;
	double bM_postprov;
	int discountTDBand;
	double dmdFinal;
	String collecName;
	int rack_id;
	double td;
	double td_new;
	double td_new_weak;
	double td_new_strong;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStyle_id() {
		return style_id;
	}
	public void setStyle_id(String style_id) {
		this.style_id = style_id;
	}
	public double getbM_postprov() {
		return bM_postprov;
	}
	public void setbM_postprov(double bM_postprov) {
		this.bM_postprov = bM_postprov;
	}
	public int getDiscountTDBand() {
		return discountTDBand;
	}
	public void setDiscountTDBand(int discountTDBand) {
		this.discountTDBand = discountTDBand;
	}
	public double getDmdFinal() {
		return dmdFinal;
	}
	public void setDmdFinal(double dmdFinal) {
		this.dmdFinal = dmdFinal;
	}
	public String getCollecName() {
		return collecName;
	}
	public void setCollecName(String collecName) {
		this.collecName = collecName;
	}
	public int getRack_id() {
		return rack_id;
	}
	public void setRack_id(int rack_id) {
		this.rack_id = rack_id;
	}
	public double getTd() {
		return td;
	}
	public void setTd(double td) {
		this.td = td;
	}
	public double getTd_new() {
		return td_new;
	}
	public void setTd_new(double td_new) {
		this.td_new = td_new;
	}
	public double getTd_new_weak() {
		return td_new_weak;
	}
	public void setTd_new_weak(double td_new_weak) {
		this.td_new_weak = td_new_weak;
	}
	public double getTd_new_strong() {
		return td_new_strong;
	}
	public void setTd_new_strong(double td_new_strong) {
		this.td_new_strong = td_new_strong;
	}
	
	
}
