package com.myntra.vo;

public class ElasticityRackVo {

	String styleId;
	String articleType;
	String gender;
	String collection;
	String rack;
	int elasticity;
	String cascElasticity_unif = "NA";
	String cascElasticity_TD = "NA";
	String cascElasticity_CD = "NA";
	String cascVisibility_Elasticity = "NA";
	String elasticityType;

	public String getStyleId() {
		return styleId;
	}
	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}
	public String getArticleType() {
		return articleType;
	}
	public void setArticleType(String articleType) {
		this.articleType = articleType;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	public String getRack() {
		return rack;
	}
	public void setRack(String rack) {
		this.rack = rack;
	}
	public int getElasticity() {
		return elasticity;
	}
	public void setElasticity(int elasticity) {
		this.elasticity = elasticity;
	}
	public String getCascElasticity_unif() {
		return cascElasticity_unif;
	}
	public void setCascElasticity_unif(String cascElasticity_unif) {
		this.cascElasticity_unif = cascElasticity_unif;
	}
	public String getCascElasticity_TD() {
		return cascElasticity_TD;
	}
	public void setCascElasticity_TD(String cascElasticity_TD) {
		this.cascElasticity_TD = cascElasticity_TD;
	}
	public String getCascElasticity_CD() {
		return cascElasticity_CD;
	}
	public void setCascElasticity_CD(String cascElasticity_CD) {
		this.cascElasticity_CD = cascElasticity_CD;
	}
	public String getCascVisibility_Elasticity() {
		return cascVisibility_Elasticity;
	}
	public void setCascVisibility_Elasticity(String cascVisibility_Elasticity) {
		this.cascVisibility_Elasticity = cascVisibility_Elasticity;
	}
	public String getElasticityType() {
		return elasticityType;
	}
	public void setElasticityType(String elasticityType) {
		this.elasticityType = elasticityType;
	}

}
