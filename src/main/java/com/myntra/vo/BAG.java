package com.myntra.vo;

/**
 * Created by 11138 on 03/05/17.
 */
public class BAG {
    private String brand;
    private String article_type;
    private String gender;

    public BAG(String brand, String article_type, String gender) {
        this.brand = brand;
        this.article_type = article_type;
        this.gender = gender;
    }

    public String getBrand() {

        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getArticle_type() {
        return article_type;
    }

    public void setArticle_type(String article_type) {
        this.article_type = article_type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
