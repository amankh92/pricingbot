package com.myntra.vo;

public class AllStyleDetails {
	
    String actiontype;
    String category;
    String styleid;
    double currentdiscount;
    double maxdiscount;
    double mindiscount;
    double newdiscount;
    double totalscore;
    double dmdFinal;
    int elasticityBand;
    int norm_adj_bm_breach;
    double netScore;
    
	public double getNetScore() {
		return netScore;
	}
	public void setNetScore(double netScore) {
		this.netScore = netScore;
	}
	public String getActiontype() {
		return actiontype;
	}
	public void setActiontype(String actiontype) {
		this.actiontype = actiontype;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getStyleid() {
		return styleid;
	}
	public void setStyleid(String styleid) {
		this.styleid = styleid;
	}
	public double getCurrentdiscount() {
		return currentdiscount;
	}
	public void setCurrentdiscount(double currentdiscount) {
		this.currentdiscount = currentdiscount;
	}
	public double getMaxdiscount() {
		return maxdiscount;
	}
	public void setMaxdiscount(double maxdiscount) {
		this.maxdiscount = maxdiscount;
	}
	public double getMindiscount() {
		return mindiscount;
	}
	public void setMindiscount(double mindiscount) {
		this.mindiscount = mindiscount;
	}
	public double getNewdiscount() {
		return newdiscount;
	}
	public void setNewdiscount(double newdiscount) {
		this.newdiscount = newdiscount;
	}
	public double getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(double totalscore) {
		this.totalscore = totalscore;
	}
	public double getDmdFinal() {
		return dmdFinal;
	}
	public void setDmdFinal(double dmdFinal) {
		this.dmdFinal = dmdFinal;
	}
	public int getElasticityBand() {
		return elasticityBand;
	}
	public void setElasticityBand(int elasticityBand) {
		this.elasticityBand = elasticityBand;
	}
	public int getNorm_adj_bm_breach() {
		return norm_adj_bm_breach;
	}
	public void setNorm_adj_bm_breach(int norm_adj_bm_breach) {
		this.norm_adj_bm_breach = norm_adj_bm_breach;
	}
    
}
