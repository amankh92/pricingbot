package com.myntra.main;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

import com.datastax.driver.core.ResultSet;
import com.myntra.service.EmailService;
import com.myntra.sql.connector.RedShiftConnectionFactory;
import com.myntra.vo.*;
import javafx.beans.binding.IntegerBinding;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.myntra.service.EmptyCSAlertSender;
import com.myntra.sql.connector.CassandraConnector;
import com.myntra.util.FileElasticity;
import com.myntra.util.NetscoreComparatorForAllStyles;
import com.myntra.util.RankComparator;
import com.myntra.util.Utility;


public class PricingAlternate {


    static String optimalStylesSql = "select style, confidence, action, gender, category, current_discount, optimal_discount, rackname from live_tracker.discount_temp where confidence in (%s) and action = %s ";

    private static CassandraConnector cassandraConnector = new CassandraConnector();

    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    static SimpleDateFormat timeFormat = new SimpleDateFormat("HH");

    private static Logger logger = LoggerFactory.getLogger(PricingJeans.class);

    public static void main(String[] args) throws Exception{

        System.out.println("Running DiscountChange");
        logger.debug("Running DiscountChange");

        final Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("./config.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        final String[] cassandraIP = properties.getProperty("cassandraHosts").split(",");
        final String[] emailRecipients = properties.getProperty("emailRecipients").split(",");

        ArrayList<BAG> bagList = new ArrayList<BAG>();

        if (args.length > 0){
            BAG category = new BAG(args[0], args[1], args[2]);
            bagList.add(category);
        }
        else{
            bagList = readBagCSV("./categories.csv");
        }

        FileElasticity.calculateFileElasticity();

        final Session session = cassandraConnector.getSession(cassandraIP, 9042);

        Date date = new Date();
        final int currentDate = Integer.parseInt(dateFormat.format(date));
        int currentTimeInitial = Integer.parseInt(timeFormat.format(date)) * 100 + 30;

        final int actualCurrentTime = currentTimeInitial;

        System.out.println("Process Started for time :" + currentDate + ":" + currentTimeInitial);
        logger.debug("Process Started for time :" + currentDate + ":" + currentTimeInitial);


        final Map<String, ElasticityRackVo> styleRackElasticityMap = getStyleRackElasticity(session);
        System.out.println("rack map size: " + styleRackElasticityMap.size());

        Map<String, StyleDetails> computedSnapShotMapInitial = ComputedSnapshotReader.readComputedSnapshot(session, currentDate, currentTimeInitial);

        if (computedSnapShotMapInitial.size() <= 200000) {
            EmptyCSAlertSender.sendEmail(currentDate, currentTimeInitial);
            do {
                currentTimeInitial = currentTimeInitial - 100;
                System.out.println("Do currentTime : " + currentTimeInitial);
                computedSnapShotMapInitial = ComputedSnapshotReader.readComputedSnapshot(session, currentDate, currentTimeInitial);
            } while (computedSnapShotMapInitial.size() <= 200000);

        }

        final int currentTime = currentTimeInitial;
        final Map<String, StyleDetails> computedSnapShotMap = computedSnapShotMapInitial;
        System.out.println("Computed Snapshot Size: " + computedSnapShotMap.size());

        Thread threads[] = new Thread[bagList.size()];

        for (int i=0; i < bagList.size(); i++){
            final BAG bag = bagList.get(i);
            final int counter = i;
            threads[i] = new Thread() {
                String article_type;
                String gender;
                String brand;
                public void run(){

                    brand = bag.getBrand();
                    article_type = bag.getArticle_type();
                    gender = bag.getGender();

                    Map<String, Contributions> tlp_contributions = new HashMap<String, Contributions>();
                    Map<String, StyleDetails> modamap = filterCategoryStyles(computedSnapShotMap, brand, article_type, gender);
                    System.out.println("Sie of Map: " + modamap.size());


                    Map<String, Map<String, StyleDetails>> newFileMap1 = runProcess("{\"category\":\"All\",\"discountArr\":[{\"band\":0,\"discount\":5},{\"band\":3,\"discount\":5},{\"band\":4,\"discount\":5}],\"highDemand\":false,\"pushAction\":false,\"bumpAction\":1}", session, currentDate, currentTime, tlp_contributions, properties, styleRackElasticityMap, modamap, brand);
                    Map<String, StyleDetails> bigDataMap1 = newFileMap1.get("dataMap");
                    //Adding logic to change discount with maximum deflection of 15
                    bigDataMap1 = Utility.final15Check(1, bigDataMap1);
                    //1 is for bumpRevenue
                    List<File> file1 = FilesGenerator.generateFile(1, currentDate, currentTime, bigDataMap1, brand, article_type, gender);


                    Map<String, Map<String, StyleDetails>> newFileMap2 = runProcess("{\"category\":\"All\",\"discountArr\":[{\"band\":0,\"discount\":-5},{\"band\":1,\"discount\":-5},{\"band\":2,\"discount\":-5}],\"highDemand\":false,\"pushAction\":false,\"bumpAction\":-1}", session, currentDate, currentTime, tlp_contributions, properties, styleRackElasticityMap, modamap, brand);
                    Map<String, StyleDetails> bigDataMap2 = newFileMap2.get("dataMap");
                    bigDataMap2 = Utility.final15Check(-1, bigDataMap2);
                    //-1 is for bumpMargin
                    List<File> file2 = FilesGenerator.generateFile(-1, currentDate, currentTime, bigDataMap2, brand, article_type, gender);

                    logger.debug("sending email");

                    EmailService emailService = new EmailService();
                    emailService.sendEmail(file1, file2, currentDate, currentTime, actualCurrentTime, brand, article_type, gender, emailRecipients);
                }
            };
            threads[i].start();
        }
        for (int i = 0; i < threads.length; i++){
            threads[i].join();
        }

        session.close();
        cassandraConnector.close();

        System.out.println("Process Completed");
        logger.debug("Process Completed");
    }

    public static Map<String, Map<String, StyleDetails>> runProcess(String json, Session session, int currentDate, int currentTime, Map<String, Contributions> tlp_contributions, Properties properties, Map<String, ElasticityRackVo> styleRackElasticityMap, Map<String, StyleDetails> computedSnapShotMap, String brand) {

        Map<Integer, Double> elasticityDiscount = new HashMap<Integer, Double>();

        Map<String, Double> mcBigRatioMap = new HashMap<String, Double>();

        JSONObject jsonObject = null;
        boolean pushAction = false;
        boolean highDemand = false;
        int bumpAction = 0;
        try {
            jsonObject = new JSONObject(json);
            bumpAction = jsonObject.getInt("bumpAction");
            JSONArray discountArr = jsonObject.getJSONArray("discountArr");
            for (int i = 0; i < discountArr.length(); i++) {
                JSONObject bandDiscount = discountArr.getJSONObject(i);
                elasticityDiscount.put(bandDiscount.getInt("band"), bandDiscount.getDouble("discount"));
            }
            highDemand = jsonObject.getBoolean("highDemand");
            pushAction = jsonObject.getBoolean("pushAction");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Map<String, Map<String, StyleDetails>> resultMap = null;

        try {
            resultMap = validateAllDiscountPercentAndSendMail(bumpAction, elasticityDiscount, pushAction, highDemand, session, currentDate, currentTime, tlp_contributions, properties, styleRackElasticityMap, computedSnapShotMap, mcBigRatioMap, brand);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.exit(0);
        }


        return resultMap;
    }

    private static void addElasticityValues(Map<String, StyleDetails> computedSnapShotMap, Map<String, ElasticityRackVo> styleRackElasticityMap){
        for (String str : computedSnapShotMap.keySet()){
            StyleDetails sd = computedSnapShotMap.get(str);
            if (styleRackElasticityMap.containsKey(str)){
                sd.setElasticityBand(styleRackElasticityMap.get(str).getElasticity());
            }
            else {
                sd.setElasticityBand(0);
            }
        }
    }


    public static Map<String, Map<String, StyleDetails>> validateAllDiscountPercentAndSendMail(int bumpAction, Map<Integer, Double> elasticityDiscount, boolean pushAction, boolean highDemand, Session session, int currentDate, int currentTime, Map<String, Contributions> tlp_contributions, Properties properties, Map<String, ElasticityRackVo> styleRackElasticityMap, Map<String, StyleDetails> computedSnapShotMap, Map<String, Double> mcBigRatioMap, String brand) throws Exception {

        addElasticityValues(computedSnapShotMap, styleRackElasticityMap);


        Map<String, StyleDetails> zeroElasticityMap = filterZeroElasticityStyles(computedSnapShotMap);

        Map<String, StyleDetails> sortedComputedSnapshot = Utility.sortMap(zeroElasticityMap, bumpAction);  //NetscoreComparatorForStyleDetails


        Map<String, StyleDetails> selectedComputedSnapshot = new LinkedHashMap<String, StyleDetails>();

        int bumpRevenuePerc = Integer.parseInt(properties.getProperty("bumpRevenuePerc"));
        int bumpMarginPerc = Integer.parseInt(properties.getProperty("bumpMarginPerc"));

        int bumpRevenueCount = zeroElasticityMap.size() * bumpRevenuePerc / 100;
        int bumpMarginCount = zeroElasticityMap.size() * bumpMarginPerc / 100;

        int i = 0;
        for (Map.Entry<String, StyleDetails> entry : computedSnapShotMap.entrySet()) {
            StyleDetails styleDetails = entry.getValue();
            if (bumpAction == 1) {
                if (styleDetails.getElasticityBand() >= 3)
                    selectedComputedSnapshot.put(entry.getKey(), styleDetails);
            }
            else if (bumpAction == -1){
                if (styleDetails.getElasticityBand() > 0 && styleDetails.getElasticityBand() <= 2){
                    selectedComputedSnapshot.put(entry.getKey(), styleDetails);
                }
            }
        }

        System.out.println("size of zero elasticity map: " + zeroElasticityMap.size());
        for (Map.Entry<String, StyleDetails> entry: zeroElasticityMap.entrySet()){
            StyleDetails styleDetails = entry.getValue();
            if ((bumpAction == 1 && i < bumpRevenueCount) || (bumpAction == -1 && i < bumpMarginCount)){
                selectedComputedSnapshot.put(entry.getKey(), styleDetails);
                i++;
            }
        }

        System.out.println("Selected computed snapshot size : " + selectedComputedSnapshot.size());

        System.out.println("sortedComputedSnapshot.size() :  " + sortedComputedSnapshot.size() + "     selectedComputedSnapshot : " + selectedComputedSnapshot.size());

        boolean firstCall = tableUpdateStatus(session);

        Map<String, StyleDetails> dataMap = new HashMap<String, StyleDetails>();

        Contributions bigContributions = new Contributions();

        int dataMapOptimalsCount = 0;

        String[] optimalsConfidenceArr = {"4", "3", "2"};
        int optimalsIndex = 0;

        Map<String, AllStyleDetails> mcMap = MCReader.readMC(session, elasticityDiscount, computedSnapShotMap, highDemand, firstCall);

        Map<String, AllStyleDetails> sortedMCMap = Utility.sortAllStyleDetailsMap(mcMap);  //DmdFinalComparator

        //********************************
        int commonCount = 0;

        for (Map.Entry<String, AllStyleDetails> entry : sortedMCMap.entrySet()) {
            if (selectedComputedSnapshot.containsKey(entry.getKey())) {
                commonCount++;
            }
        }

        System.out.println("MC styles in 90K   :   " + commonCount);

        //********************************

        for (Map.Entry<String, AllStyleDetails> entry : sortedMCMap.entrySet()) {

            if (!dataMap.containsKey(entry.getKey()) && selectedComputedSnapshot.containsKey(entry.getKey())) {

                AllStyleDetails mcObj = entry.getValue();

                StyleDetails st = new StyleDetails();

                StyleDetails styleComputedDetails = selectedComputedSnapshot.get(entry.getKey());

                st.setStyleid(entry.getKey());
                st.setCurrentTd(mcObj.getCurrentdiscount());

                double newTd = mcObj.getNewdiscount();

                if (newTd > styleComputedDetails.getNorm_adj_bm_new()) {
                    st.setNewTd(styleComputedDetails.getNorm_adj_bm_new());
                    newTd = styleComputedDetails.getNorm_adj_bm_new();
                }

                if ((newTd <= styleComputedDetails.getMaxDiscount()) && (newTd >= styleComputedDetails.getMinDiscount())) {
                    st.setNewTd(newTd);
                } else {
                    if (newTd < styleComputedDetails.getMinDiscount()) {
                        st.setNewTd(styleComputedDetails.getMinDiscount());
                    }
                    if (newTd > styleComputedDetails.getMaxDiscount()) {
                        st.setNewTd(styleComputedDetails.getMaxDiscount());
                    }
                }

                st.setMinDiscount(styleComputedDetails.getMinDiscount());
                st.setMaxDiscount(styleComputedDetails.getMaxDiscount());
                st.setTotalscore(styleComputedDetails.getTotalscore());
                st.setDmdfinal(styleComputedDetails.getDmdfinal());
                st.setArticlemrp(styleComputedDetails.getArticlemrp());
                st.setArticletype(styleComputedDetails.getArticletype());
                st.setGender(styleComputedDetails.getGender());
                st.setDiscountfunding(styleComputedDetails.getDiscountfunding());
                st.setFundingpercentage(styleComputedDetails.getFundingpercentage());
                st.setDiscountlimit(styleComputedDetails.getDiscountlimit());
                st.setFrom_unixtime_expired_on(styleComputedDetails.getFrom_unixtime_expired_on());
                st.setGetcount(styleComputedDetails.getGetcount());
                st.setGetamount(styleComputedDetails.getGetamount());
                st.setBrand(styleComputedDetails.getBrand());
                st.setDiscountid(styleComputedDetails.getDiscountid());
                st.setBrandtype(styleComputedDetails.getBrandtype());
                st.setBusinessunit(styleComputedDetails.getBusinessunit());
                st.setCurrentcommercialtype(styleComputedDetails.getCurrentcommercialtype());
                st.setNorm_adj_bm(styleComputedDetails.getNorm_adj_bm());
                st.setNorm_adj_bm_new(styleComputedDetails.getNorm_adj_bm_new());
                st.setNorm_adj_bm_new_actual(styleComputedDetails.getNorm_adj_bm_new_actual());
                st.setNetscore(styleComputedDetails.getNetscore());
                st.setTotalscore(styleComputedDetails.getTotalscore());
                st.setAverageage(styleComputedDetails.getAverageage());
                st.setSeasoncode(styleComputedDetails.getSeasoncode());
                st.setElasticityBand(styleComputedDetails.getElasticityBand());
                dataMap.put(st.getStyleid(), st);
            }
        }

        System.out.println("DataMap size   2    :   " + dataMap.size());

        bigContributions.setMc(dataMap.size() - bigContributions.getHealth());

        System.out.println("Optimals from non first call");

        while (dataMap.size() < 800 && optimalsIndex < optimalsConfidenceArr.length) {
            System.out.println("optimalIndex : " + optimalsIndex + " optimal Confidence : " + optimalsConfidenceArr[optimalsIndex]);

            Map<String, CategoryStyleRow> optimalsMap = getOptimals(session, bumpAction, optimalsConfidenceArr[optimalsIndex]);

            for (Map.Entry<String, CategoryStyleRow> entry : optimalsMap.entrySet()) {
                if (!dataMap.containsKey(entry.getKey()) && selectedComputedSnapshot.containsKey(entry.getKey())) {
                    StyleDetails st = new StyleDetails();

                    CategoryStyleRow categoryStyleRow = entry.getValue();

                    StyleDetails styleComputedDetails = selectedComputedSnapshot.get(entry.getKey());

                    st.setStyleid(entry.getKey());

                    st.setCurrentTd(categoryStyleRow.getTd());

                    double newTd = categoryStyleRow.getTd_new();

                    if (newTd > styleComputedDetails.getNorm_adj_bm_new()) {
                        st.setNewTd(styleComputedDetails.getNorm_adj_bm_new());
                        newTd = styleComputedDetails.getNorm_adj_bm_new();
                    }

                    if ((newTd <= styleComputedDetails.getMaxDiscount()) && (newTd >= styleComputedDetails.getMinDiscount())) {
                        st.setNewTd(newTd);
                    } else {
                        if (newTd < styleComputedDetails.getMinDiscount()) {
                            st.setNewTd(styleComputedDetails.getMinDiscount());
                        }
                        if (newTd > styleComputedDetails.getMaxDiscount()) {
                            st.setNewTd(styleComputedDetails.getMaxDiscount());
                        }
                    }

                    st.setMinDiscount(styleComputedDetails.getMinDiscount());
                    st.setMaxDiscount(styleComputedDetails.getMaxDiscount());
                    st.setTotalscore(styleComputedDetails.getTotalscore());
                    st.setDmdfinal(styleComputedDetails.getDmdfinal());
                    st.setArticlemrp(styleComputedDetails.getArticlemrp());
                    st.setArticletype(styleComputedDetails.getArticletype());
                    st.setGender(styleComputedDetails.getGender());
                    st.setDiscountfunding(styleComputedDetails.getDiscountfunding());
                    st.setFundingpercentage(styleComputedDetails.getFundingpercentage());
                    st.setDiscountlimit(styleComputedDetails.getDiscountlimit());
                    st.setFrom_unixtime_expired_on(styleComputedDetails.getFrom_unixtime_expired_on());
                    st.setGetcount(styleComputedDetails.getGetcount());
                    st.setGetamount(styleComputedDetails.getGetamount());
                    st.setBrand(styleComputedDetails.getBrand());
                    st.setDiscountid(styleComputedDetails.getDiscountid());
                    st.setBrandtype(styleComputedDetails.getBrandtype());
                    st.setBusinessunit(styleComputedDetails.getBusinessunit());
                    st.setCurrentcommercialtype(styleComputedDetails.getCurrentcommercialtype());
                    st.setNorm_adj_bm(styleComputedDetails.getNorm_adj_bm());
                    st.setNorm_adj_bm_new(styleComputedDetails.getNorm_adj_bm_new());
                    st.setNorm_adj_bm_new_actual(styleComputedDetails.getNorm_adj_bm_new_actual());
                    st.setNetscore(styleComputedDetails.getNetscore());
                    st.setTotalscore(styleComputedDetails.getTotalscore());
                    st.setAverageage(styleComputedDetails.getAverageage());
                    st.setSeasoncode(styleComputedDetails.getSeasoncode());
                    st.setElasticityBand(styleComputedDetails.getElasticityBand());

                    if ((bumpAction == 1 && st.getNewTd() > st.getCurrentTd()) || (bumpAction == -1 && st.getNewTd() < st.getCurrentTd())) {
                        if (dataMapOptimalsCount < 800) {
                            st.setHealth(1);
                            dataMapOptimalsCount++;
                            dataMap.put(st.getStyleid(), st);
                        }
                    }
                }
            }
            optimalsIndex++;
        }
        System.out.println("DataMap size   3    :   " + dataMap.size());

        bigContributions.setHealth(dataMap.size() - bigContributions.getMc());


        int mcNetscoreCount = 0;
        int mcNetscoreBigCount = (int) ((45000 - dataMap.size()) / 2);

        System.out.println("  selectedComputedSnapshot.size    :    " + selectedComputedSnapshot.size());

        for (Map.Entry<String, StyleDetails> entry : selectedComputedSnapshot.entrySet()) {

            if (!dataMap.containsKey(entry.getKey()))/*
					&& ((FileElasticity.fileStyleElasticity.containsKey(entry.getKey()) && elasticityDiscount.containsKey(FileElasticity.fileStyleElasticity.get(entry.getKey()).getElasticity()))
							|| (styleRackElasticityMap.containsKey(entry.getKey()) && elasticityDiscount.containsKey(styleRackElasticityMap.get(entry.getKey()).getElasticity()))
							))*/ {

                StyleDetails st = new StyleDetails();

                StyleDetails vo = entry.getValue();
                st.setStyleid(entry.getKey());
                st.setCurrentTd(Double.parseDouble(vo.getGetpercent()));

                if (bumpAction == 1) {
                    st.setNewTd(((int) st.getCurrentTd() / 5) * 5 + 5);
                } else {
                    double newTd = ((int) st.getCurrentTd() / 5) * 5;
                    if (newTd == st.getCurrentTd()) {
                        st.setNewTd(newTd - 5);
                    } else {
                        st.setNewTd(newTd);
                    }
                }

                st.setMinDiscount(vo.getMinDiscount());
                st.setMaxDiscount(vo.getMaxDiscount());
                st.setTotalscore(vo.getTotalscore());
                st.setDmdfinal(vo.getDmdfinal());
                st.setArticlemrp(vo.getArticlemrp());
                st.setArticletype(vo.getArticletype());
                st.setGender(vo.getGender());
                st.setDiscountfunding(vo.getDiscountfunding());
                st.setFundingpercentage(vo.getFundingpercentage());
                st.setDiscountlimit(vo.getDiscountlimit());
                st.setFrom_unixtime_expired_on(vo.getFrom_unixtime_expired_on());
                st.setGetcount(vo.getGetcount());
                st.setGetamount(vo.getGetamount());
                st.setBrand(vo.getBrand());
                st.setDiscountid(vo.getDiscountid());
                st.setBrandtype(vo.getBrandtype());
                st.setBusinessunit(vo.getBusinessunit());
                st.setCurrentcommercialtype(vo.getCurrentcommercialtype());
                st.setNorm_adj_bm(vo.getNorm_adj_bm());
                st.setNorm_adj_bm_new(vo.getNorm_adj_bm_new());
                st.setNorm_adj_bm_new_actual(vo.getNorm_adj_bm_new_actual());
                st.setNorm_adj_bm_breach(vo.getNorm_adj_bm_breach());
                st.setNetscore(vo.getNetscore());
                st.setTotalscore(vo.getTotalscore());
                st.setAverageage(vo.getAverageage());
                st.setSeasoncode(vo.getSeasoncode());
                st.setElasticityBand(vo.getElasticityBand());

                if (st.getNewTd() > vo.getNorm_adj_bm_new()) {
                    st.setNewTd(vo.getNorm_adj_bm_new());
                }

                if ((st.getNewTd() <= st.getMaxDiscount()) && (st.getNewTd() >= st.getMinDiscount())) {
                    //st.setNewTd(newTd);
                } else {
                    if (st.getNewTd() < st.getMinDiscount()) {
                        st.setNewTd(st.getMinDiscount());
                    }
                    if (st.getNewTd() > st.getMaxDiscount()) {
                        st.setNewTd(st.getMaxDiscount());
                    }
                }

                if ((bumpAction == 1 && st.getNewTd() > st.getCurrentTd()) || (bumpAction == -1 && st.getNewTd() < st.getCurrentTd())) {
                    if (mcNetscoreCount < mcNetscoreBigCount) {
                        dataMap.put(st.getStyleid(), st);
                        mcNetscoreCount++;
                    }
                }

            }
        }
        System.out.println("DataMap size   4    :   " + dataMap.size());

        Map<String, StyleDetails> dmdFinalSortedComputedSnapshot = Utility.sortStyleDetailsByDmdfinal(selectedComputedSnapshot);

        for (Map.Entry<String, StyleDetails> entry : dmdFinalSortedComputedSnapshot.entrySet()) {

            if (!dataMap.containsKey(entry.getKey()))/*
					&& ((FileElasticity.fileStyleElasticity.containsKey(entry.getKey()) && elasticityDiscount.containsKey(FileElasticity.fileStyleElasticity.get(entry.getKey()).getElasticity()))
							|| (styleRackElasticityMap.containsKey(entry.getKey()) && elasticityDiscount.containsKey(styleRackElasticityMap.get(entry.getKey()).getElasticity()))
							))*/ {


                StyleDetails st = new StyleDetails();

                StyleDetails vo = entry.getValue();
                st.setStyleid(entry.getKey());
                st.setCurrentTd(Double.parseDouble(vo.getGetpercent()));
                if (bumpAction == 1) {
                    st.setNewTd(((int) st.getCurrentTd() / 5) * 5 + 5);
                } else {
                    double newTd = ((int) st.getCurrentTd() / 5) * 5;
                    if (newTd == st.getCurrentTd()) {
                        st.setNewTd(newTd - 5);
                    } else {
                        st.setNewTd(newTd);
                    }
                }
                st.setMinDiscount(vo.getMinDiscount());
                st.setMaxDiscount(vo.getMaxDiscount());
                st.setTotalscore(vo.getTotalscore());
                st.setDmdfinal(vo.getDmdfinal());
                st.setArticlemrp(vo.getArticlemrp());
                st.setArticletype(vo.getArticletype());
                st.setGender(vo.getGender());
                st.setDiscountfunding(vo.getDiscountfunding());
                st.setFundingpercentage(vo.getFundingpercentage());
                st.setDiscountlimit(vo.getDiscountlimit());
                st.setFrom_unixtime_expired_on(vo.getFrom_unixtime_expired_on());
                st.setGetcount(vo.getGetcount());
                st.setGetamount(vo.getGetamount());
                st.setBrand(vo.getBrand());
                st.setDiscountid(vo.getDiscountid());
                st.setBrandtype(vo.getBrandtype());
                st.setBusinessunit(vo.getBusinessunit());
                st.setCurrentcommercialtype(vo.getCurrentcommercialtype());
                st.setNorm_adj_bm(vo.getNorm_adj_bm());
                st.setNorm_adj_bm_new(vo.getNorm_adj_bm_new());
                st.setNorm_adj_bm_new_actual(vo.getNorm_adj_bm_new_actual());
                st.setNorm_adj_bm_breach(vo.getNorm_adj_bm_breach());
                st.setNetscore(vo.getNetscore());
                st.setTotalscore(vo.getTotalscore());
                st.setAverageage(vo.getAverageage());
                st.setSeasoncode(vo.getSeasoncode());
                st.setElasticityBand(vo.getElasticityBand());
                //*****
                if (st.getNewTd() > vo.getNorm_adj_bm_new()) {
                    st.setNewTd(vo.getNorm_adj_bm_new());
                }

                if ((st.getNewTd() <= st.getMaxDiscount()) && (st.getNewTd() >= st.getMinDiscount())) {
                    //st.setNewTd(newTd);
                } else {
                    if (st.getNewTd() < st.getMinDiscount()) {
                        st.setNewTd(st.getMinDiscount());
                    }
                    if (st.getNewTd() > st.getMaxDiscount()) {
                        st.setNewTd(st.getMaxDiscount());
                    }
                }


                if ((bumpAction == 1 && st.getNewTd() > st.getCurrentTd()) || (bumpAction == -1 && st.getNewTd() < st.getCurrentTd())) {

					/*if (mcDmdfinalCount < 2000) {
						smallDataMap.put(st.getStyleid(), st);
					}

					if (mcDmdfinalCount < 7500) {
						dataMap.put(st.getStyleid(), st);
						mcDmdfinalCount++;
					}*/
                    if (dataMap.size() < 45000) {
                        dataMap.put(st.getStyleid(), st);
                    }

                }

            }
        }

        System.out.println("DataMap size   5    :   " + dataMap.size());

        dataMap = Utility.sortStyleDetailsByDmdfinal(dataMap);

        if (currentTime > 1000 && currentTime < 1600) {
            updateMCReplaceAndAdd(bumpAction, dataMap, selectedComputedSnapshot, mcMap);
        }

//        addElasticityValues(dataMap, styleRackElasticityMap);
        discountCorrections(dataMap, bumpAction);
//        dataMap = getStyleTrunk(dataMap, bumpAction);
        dataMap = removeDiscountBreaches(dataMap, bumpAction);
        dataMap = removeUnchangedDiscountStyles(dataMap);

        Map<String, Map<String, StyleDetails>> fileMap = new HashMap<String, Map<String, StyleDetails>>();
        fileMap.put("dataMap", dataMap);

        return fileMap;
    }

    public static Map<String, StyleDetails> filterZeroElasticityStyles(Map<String, StyleDetails> map){
        Map<String, StyleDetails> filteredMap = new HashMap<String, StyleDetails>();
        for (Map.Entry<String, StyleDetails> entry: map.entrySet()){
//            System.out.println(entry.getValue().getElasticityBand());
            if (entry.getValue().getElasticityBand() == 0){
//                System.out.println("here");
                filteredMap.put(entry.getKey(), entry.getValue());
            }
        }
        return filteredMap;
    }

    private static Map<String, StyleDetails> removeDiscountBreaches(Map<String, StyleDetails> map, int bumpAction) {
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String style: map.keySet()){
            if (bumpAction == 1) {
                if (map.get(style).getNewTd() <= map.get(style).getMaxDiscount() && map.get(style).getCurrentTd() <= 50){
                    newMap.put(style, map.get(style));
                }
            }
            else {
                if (map.get(style).getNewTd() <= map.get(style).getMaxDiscount()){
                    newMap.put(style, map.get(style));
                }
            }
        }
        return newMap;
    }

    private static Map<String, StyleDetails> finalList(Map<String, StyleDetails> map, int bumpAction){
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String str : map.keySet()){
            if (bumpAction == 1){
                if (map.get(str).getElasticityBand()!=1 && map.get(str).getElasticityBand()!=2){
                    newMap.put(str, map.get(str));
                }
            }
            else {
                if (map.get(str).getElasticityBand()!=3 && map.get(str).getElasticityBand()!=4){
                    newMap.put(str, map.get(str));
                }
            }
        }
        return newMap;
    }

    private static Map<String, StyleDetails> selectStyles(Map<String, StyleDetails> map){
        int counter = 0;
        int index05percentile = (int) (map.size() * 0.05);
        int index40percentile = (int) (map.size() * 0.40);
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String str : map.keySet()) {
            if ((counter < index05percentile && counter > index40percentile)) {
                newMap.put(str, map.get(str));
            }
            counter++;
        }
        return newMap;
    }

    private static Map<String, StyleDetails> getTopStyles(Map<String, StyleDetails> map, int bumpAction){
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        map = removeHighDemandStyles(map);
        int i = 0;
        for (String str : map.keySet()){
//            if (i < 80) {
            if (bumpAction == 1) {
                if (map.get(str).getElasticityBand()!=1 && map.get(str).getElasticityBand()!=2){
                    newMap.put(str, map.get(str));
                    i++;
                }
            }
            else {
                if (map.get(str).getElasticityBand()!=3 && map.get(str).getElasticityBand()!=4){
                    newMap.put(str, map.get(str));
                    i++;
                }
            }
//            }
        }
        return newMap;
    }

    private static Map<String, StyleDetails> removeHighDemandStyles(Map<String, StyleDetails> map){
        int counter = 0;
        int index95percentile = (int) (map.size() * 0.05);
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String str : map.keySet()) {
            if ((counter > index95percentile)) {
                newMap.put(str, map.get(str));
            }
            counter++;
        }
        return newMap;
    }

    private static Map<String, StyleDetails> tempPush(Map<String, StyleDetails> map){
        Map<String, StyleDetails> newMap1 = new LinkedHashMap<String, StyleDetails>();
        int i = 0;
        int j = 0;
        for (String str : map.keySet()) {
            if (i < 20) {
                if (map.get(str).getElasticityBand() == 4 && map.get(str).getCurrentTd() != 0) {
                    newMap1.put(str, map.get(str));
                    i++;
                }
            }
            if (j < 20) {
                if (map.get(str).getCurrentTd() == 0 && map.get(str).getElasticityBand() >= 3) {
                    newMap1.put(str, map.get(str));
                    j++;
                }
            }
        }
        i = newMap1.size() - 1;
        for (String str : map.keySet()){
            if (i < 50){
                if (!newMap1.containsKey(str) && map.get(str).getElasticityBand() == 0)
                {
                    newMap1.put(str, map.get(str));
                    i++;
                }
            }
        }
        return newMap1;
    }

    private static void discountCorrections(Map<String, StyleDetails> map, int bumpAction){
        for (String str : map.keySet()) {
            if (map.get(str).getMaxDiscount() == 0){
                double currentTD = map.get(str).getCurrentTd();
                if (bumpAction == 1){
                    map.get(str).setNewTd(currentTD + 5);
                }
                else {
                    map.get(str).setNewTd(currentTD - 5);
                }
            }
            if (map.get(str).getCurrentTd() == 0 && map.get(str).getElasticityBand() == 4)
            {
                map.get(str).setNewTd(10);
            }
            if (Float.parseFloat(map.get(str).getAverageage()) <= 10){
                map.get(str).setMaxDiscount(30);
            }
            else {
                map.get(str).setMaxDiscount(60);
            }
            String season_code = map.get(str).getSeasoncode();
            if (season_code != null && season_code.equals("SS17")){
                map.get(str).setMaxDiscount(50);
            }
            if (map.get(str).getBrand().equals("Anouk")){
                if (Float.parseFloat(map.get(str).getAverageage()) <= 20){
                    map.get(str).setMaxDiscount(40);
                }
            }
            else if (map.get(str).getBrand().equals("HRX by Hrithik Roshan")){
                if (Float.parseFloat(map.get(str).getAverageage()) <= 20){
                    map.get(str).setMaxDiscount(30);
                }
                if (map.get(str).getSeasoncode() != null && map.get(str).getSeasoncode().equals("FW17")){
                    map.get(str).setNewTd(0);
                }
            }
        }
    }

    private static Map<String, StyleDetails> removeUnchangedDiscountStyles(Map<String, StyleDetails> map) {
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String str : map.keySet()) {
            if (map.get(str).getNewTd() != map.get(str).getCurrentTd()) {
                newMap.put(str, map.get(str));
            }
        }
        return newMap;
    }


    private static Map<String, StyleDetails> getStyleTrunk(Map<String, StyleDetails> map, int bumpAction) {

        int counter = 0;
        int index95percentile = (int) (map.size() * 0.05);
        int index40percentile = (int) (map.size() * 0.90);
        Map<String, StyleDetails> newMap = new LinkedHashMap<String, StyleDetails>();
        for (String str : map.keySet()) {
            if (bumpAction == 1) {
                if (map.get(str).getElasticityBand() != 1 && map.get(str).getElasticityBand() != 2) {
                    newMap.put(str, map.get(str));
                }
            } else {
                if (map.get(str).getElasticityBand() != 3 && map.get(str).getElasticityBand() != 4) {
                    newMap.put(str, map.get(str));
                }
            }
        }
        return newMap;
    }

    private Map<String, StyleDetails> rankStyles(int bumpAction, Map<String, StyleDetails> dataMap, Map<String, ElasticityRackVo> styleRackElasticityMap) {
        // TODO Auto-generated method stub

        List<StyleDetails> rankedList = new ArrayList<StyleDetails>();

        for (Map.Entry<String, StyleDetails> entry : dataMap.entrySet()) {

            StyleDetails vo = entry.getValue();

            double articlemrp = Double.parseDouble(vo.getArticlemrp());

            double currentSellingPrice = articlemrp - (articlemrp * vo.getCurrentTd() / 100);
            double newSellingPrice = articlemrp - (articlemrp * vo.getNewTd() / 100);

            double cascElasticity_TD = -1;

            if (FileElasticity.fileStyleElasticity.containsKey(vo.getStyleid())
                    && Utility.isDouble(FileElasticity.fileStyleElasticity.get(vo.getStyleid()).getCascElasticity_TD())) {
                cascElasticity_TD = Double.parseDouble(FileElasticity.fileStyleElasticity.get(vo.getStyleid()).getCascElasticity_TD());
            } else if (styleRackElasticityMap.containsKey(vo.getStyleid())
                    && Utility.isDouble(styleRackElasticityMap.get(vo.getStyleid()).getCascElasticity_TD())) {
                cascElasticity_TD = Double.parseDouble(styleRackElasticityMap.get(vo.getStyleid()).getCascElasticity_TD());
            }

            double newDemand = (((vo.getCurrentTd() / 100 - vo.getNewTd() / 100) / (1 - vo.getCurrentTd() / 100)) * vo.getDmdfinal() * cascElasticity_TD) + vo.getDmdfinal();

            if (bumpAction == 1) {
                vo.setRankingFactor((newSellingPrice * newDemand) - (currentSellingPrice * vo.getDmdfinal()));
            } else if (bumpAction == -1) {
                double currentRgm = currentSellingPrice - (Double.parseDouble(vo.getArticlemrp()) - (articlemrp * vo.getNorm_adj_bm_new_actual() / 100));
                double newRgm = newSellingPrice - (Double.parseDouble(vo.getArticlemrp()) - (articlemrp * vo.getNorm_adj_bm_new_actual() / 100));
                vo.setRankingFactor((newRgm * newDemand) - (currentRgm * vo.getDmdfinal()));
            }
            vo.setNewDemand(newDemand);

            rankedList.add(vo);
        }

        Collections.sort(rankedList, new RankComparator());
        Collections.reverse(rankedList);

        Map<String, StyleDetails> rankedMap = new LinkedHashMap<String, StyleDetails>();

        for (StyleDetails obj : rankedList) {
            rankedMap.put(obj.getStyleid(), obj);
        }

        return rankedMap;
    }

    public static Map<String, CategoryStyleRow> getOptimals(Session session, int bumpAction, String confidence) {

        Map<String, CategoryStyleRow> map = new HashMap<String, CategoryStyleRow>();

        ResultSet rsOptimals = session.execute(String.format(optimalStylesSql, confidence, bumpAction));

        if (rsOptimals != null) {
            Iterator<Row> iterator = rsOptimals.iterator();
            while (iterator.hasNext()) {
                Row row = iterator.next();

                CategoryStyleRow categoryStyleRow = new CategoryStyleRow();

                categoryStyleRow.setStyle_id(String.valueOf(row.getInt("style")));
                categoryStyleRow.setTd(row.getFloat("current_discount"));
                categoryStyleRow.setTd_new(row.getFloat("optimal_discount"));

                map.put(String.valueOf(row.getInt("style")), categoryStyleRow);
            }
        }

        return map;
    }

    public static boolean tableUpdateStatus(Session session) {

        String tableUpdateInfo = "select updateflag from tlp.table_update_info where table_name = 'category_style_mapping'";

        ResultSet rs = session.execute(tableUpdateInfo);

        if (rs != null) {
            Iterator<Row> iterator = rs.iterator();
            while (iterator.hasNext()) {
                Row row = iterator.next();
                return row.getBool("updateflag");
            }
        }

        return false;
    }

    public static void updateTable(Session session) {

        String updateTableSql = "update tlp.table_update_info set updateflag = false where table_name = 'category_style_mapping'";
        session.execute(updateTableSql);
    }


    public static void insertTlpContributions(Session session, int currentDate, int currentTime, Map<String, Contributions> tlp_contributions) {

        String insertTlpContributionsSql = "insert into tlp.tlp_contributions (date, time, bumpmarginbig, bumpmarginsmall, bumprevenuebig, bumprevenuesmall) values (?, ?, ?, ?, ?, ?) ";

        String bumpmarginbig = "";
        String bumprevenuebig = "";

        ObjectMapper mapper = new ObjectMapper();

        try {
            if (tlp_contributions.containsKey("bumprevenuebig")) {
                bumprevenuebig = mapper.writeValueAsString(tlp_contributions.get("bumprevenuebig"));
            }
            if (tlp_contributions.containsKey("bumpmarginbig")) {
                bumpmarginbig = mapper.writeValueAsString(tlp_contributions.get("bumpmarginbig"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        com.datastax.driver.core.PreparedStatement preparedStatement = session.prepare(insertTlpContributionsSql);

        BoundStatement boundStatement = new BoundStatement(preparedStatement);

        session.execute(boundStatement.bind(currentDate, currentTime,
                bumpmarginbig, bumprevenuebig));

    }

    private static void updateMCReplaceAndAdd(int bumpAction, Map<String, StyleDetails> finalBigMap, Map<String, StyleDetails> selectedComputedSnapshot, Map<String, AllStyleDetails> mcMap) {

        for (Map.Entry<String, StyleDetails> entry : finalBigMap.entrySet()) {

            AllStyleDetails mcObj = mcMap.get(entry.getKey());
            StyleDetails csObj = selectedComputedSnapshot.get(entry.getKey());
            if (mcObj != null) {
                StyleDetails fileObj = entry.getValue();

                if (mcObj.getCurrentdiscount() == fileObj.getCurrentTd()) {
                    if (bumpAction == 1 && mcObj.getNewdiscount() > mcObj.getCurrentdiscount()) {

                        fileObj.setNewTd(mcObj.getNewdiscount());

                        if (fileObj.getNewTd() > csObj.getNorm_adj_bm_new()) {
                            fileObj.setNewTd(csObj.getNorm_adj_bm_new());
                        }

                        if ((fileObj.getNewTd() <= fileObj.getMaxDiscount()) && (fileObj.getNewTd() >= fileObj.getMinDiscount())) {
                            //st.setNewTd(newTd);
                        } else {
                            if (fileObj.getNewTd() < fileObj.getMinDiscount()) {
                                fileObj.setNewTd(fileObj.getMinDiscount());
                            }
                            if (fileObj.getNewTd() > fileObj.getMaxDiscount()) {
                                fileObj.setNewTd(fileObj.getMaxDiscount());
                            }
                        }
                    } else if (bumpAction == -1 && mcObj.getNewdiscount() < mcObj.getCurrentdiscount()) {

                        fileObj.setNewTd(mcObj.getNewdiscount());

                        if (fileObj.getNewTd() > csObj.getNorm_adj_bm_new()) {
                            fileObj.setNewTd(csObj.getNorm_adj_bm_new());
                        }

                        if ((fileObj.getNewTd() <= fileObj.getMaxDiscount()) && (fileObj.getNewTd() >= fileObj.getMinDiscount())) {
                            //st.setNewTd(newTd);
                        } else {
                            if (fileObj.getNewTd() < fileObj.getMinDiscount()) {
                                fileObj.setNewTd(fileObj.getMinDiscount());
                            }
                            if (fileObj.getNewTd() > fileObj.getMaxDiscount()) {
                                fileObj.setNewTd(fileObj.getMaxDiscount());
                            }
                        }
                    }

                }
            }
        }


        int mcAddCount = 0;

        Map<String, AllStyleDetails> sortedMcStyles = mcSortByNetScore(bumpAction, mcMap, selectedComputedSnapshot);

        System.out.println(sortedMcStyles.size());

        for (Map.Entry<String, AllStyleDetails> entry : sortedMcStyles.entrySet()) {

            AllStyleDetails mcObj = entry.getValue();

            StyleDetails csObj = selectedComputedSnapshot.get(mcObj.getStyleid());

            if (mcObj.getCurrentdiscount() == Double.parseDouble(csObj.getGetpercent())) {

                if (bumpAction == 1 && mcObj.getNewdiscount() > mcObj.getCurrentdiscount()) {

                    StyleDetails st = new StyleDetails();

                    st.setStyleid(mcObj.getStyleid());
                    st.setCurrentTd(mcObj.getCurrentdiscount());
                    st.setNewTd(mcObj.getNewdiscount());
                    st.setMinDiscount(mcObj.getMindiscount());
                    st.setMaxDiscount(mcObj.getMaxdiscount());
                    st.setTotalscore(mcObj.getTotalscore());
                    st.setDmdfinal(mcObj.getDmdFinal());

                    st.setArticlemrp(csObj.getArticlemrp());
                    st.setArticletype(csObj.getArticletype());
                    st.setGender(csObj.getGender());
                    st.setDiscountfunding(csObj.getDiscountfunding());
                    st.setFundingpercentage(csObj.getFundingpercentage());
                    st.setDiscountlimit(csObj.getDiscountlimit());
                    st.setFrom_unixtime_expired_on(csObj.getFrom_unixtime_expired_on());
                    st.setGetcount(csObj.getGetcount());
                    st.setGetamount(csObj.getGetamount());
                    st.setBrand(csObj.getBrand());
                    st.setDiscountid(csObj.getDiscountid());
                    st.setBrandtype(csObj.getBrandtype());
                    st.setBusinessunit(csObj.getBusinessunit());
                    st.setCurrentcommercialtype(csObj.getCurrentcommercialtype());
                    st.setNorm_adj_bm(csObj.getNorm_adj_bm());
                    st.setNorm_adj_bm_new(csObj.getNorm_adj_bm_new());
                    st.setNorm_adj_bm_breach(csObj.getNorm_adj_bm_breach());
                    st.setNorm_adj_bm_new_actual(csObj.getNorm_adj_bm_new_actual());
                    st.setNetscore(csObj.getNetscore());
                    st.setTotalscore(csObj.getTotalscore());
                    st.setAverageage(csObj.getAverageage());
                    st.setSeasoncode(csObj.getSeasoncode());

                    //*****
                    if (st.getNewTd() > csObj.getNorm_adj_bm_new()) {
                        st.setNewTd(csObj.getNorm_adj_bm_new());
                    }

                    if ((st.getNewTd() <= st.getMaxDiscount()) && (st.getNewTd() >= st.getMinDiscount())) {
                        //st.setNewTd(newTd);
                    } else {
                        if (st.getNewTd() < st.getMinDiscount()) {
                            st.setNewTd(st.getMinDiscount());
                        }
                        if (st.getNewTd() > st.getMaxDiscount()) {
                            st.setNewTd(st.getMaxDiscount());
                        }
                    }

                    //*****

                    if (st.getNewTd() > st.getCurrentTd()) {
                        if (mcAddCount < 3000) {
                            finalBigMap.put(st.getStyleid(), st);
                        }
                        mcAddCount++;
                    }

                } else if (bumpAction == -1 && mcObj.getNewdiscount() < mcObj.getCurrentdiscount()) {

                    StyleDetails st = new StyleDetails();

                    st.setStyleid(mcObj.getStyleid());
                    st.setCurrentTd(mcObj.getCurrentdiscount());
                    st.setNewTd(mcObj.getNewdiscount());
                    st.setMinDiscount(mcObj.getMindiscount());
                    st.setMaxDiscount(mcObj.getMaxdiscount());
                    st.setTotalscore(mcObj.getTotalscore());
                    st.setDmdfinal(mcObj.getDmdFinal());

                    st.setArticlemrp(csObj.getArticlemrp());
                    st.setArticletype(csObj.getArticletype());
                    st.setGender(csObj.getGender());
                    st.setDiscountfunding(csObj.getDiscountfunding());
                    st.setFundingpercentage(csObj.getFundingpercentage());
                    st.setDiscountlimit(csObj.getDiscountlimit());
                    st.setFrom_unixtime_expired_on(csObj.getFrom_unixtime_expired_on());
                    st.setGetcount(csObj.getGetcount());
                    st.setGetamount(csObj.getGetamount());
                    st.setBrand(csObj.getBrand());
                    st.setDiscountid(csObj.getDiscountid());
                    st.setBrandtype(csObj.getBrandtype());
                    st.setBusinessunit(csObj.getBusinessunit());
                    st.setCurrentcommercialtype(csObj.getCurrentcommercialtype());
                    st.setNorm_adj_bm(csObj.getNorm_adj_bm());
                    st.setNorm_adj_bm_new(csObj.getNorm_adj_bm_new());
                    st.setNorm_adj_bm_breach(csObj.getNorm_adj_bm_breach());
                    st.setNorm_adj_bm_new_actual(csObj.getNorm_adj_bm_new_actual());
                    st.setNetscore(csObj.getNetscore());
                    st.setTotalscore(csObj.getTotalscore());
                    st.setAverageage(csObj.getAverageage());
                    st.setSeasoncode(csObj.getSeasoncode());

                    //*****
                    if (st.getNewTd() > csObj.getNorm_adj_bm_new()) {
                        st.setNewTd(csObj.getNorm_adj_bm_new());
                    }

                    if ((st.getNewTd() <= st.getMaxDiscount()) && (st.getNewTd() >= st.getMinDiscount())) {
                        //st.setNewTd(newTd);
                    } else {
                        if (st.getNewTd() < st.getMinDiscount()) {
                            st.setNewTd(st.getMinDiscount());
                        }
                        if (st.getNewTd() > st.getMaxDiscount()) {
                            st.setNewTd(st.getMaxDiscount());
                        }
                    }

                    if (st.getNewTd() < st.getCurrentTd()) {
                        if (mcAddCount < 3000) {
                            finalBigMap.put(st.getStyleid(), st);
                        }
                        mcAddCount++;
                    }
                }
            }
        }
    }

    private static Map<String, AllStyleDetails> mcSortByNetScore(int bumpAction, Map<String, AllStyleDetails> mcMap, Map<String, StyleDetails> selectedComputedSnapshot) {

        List<AllStyleDetails> list = new ArrayList<AllStyleDetails>();

        for (Map.Entry<String, AllStyleDetails> entry : mcMap.entrySet()) {
            AllStyleDetails vo = entry.getValue();
            if (selectedComputedSnapshot.get(vo.getStyleid()) != null) {
                vo.setNetScore(selectedComputedSnapshot.get(vo.getStyleid()).getNetscore());
                list.add(vo);
            }
        }

        Map<String, AllStyleDetails> sortedMap = new LinkedHashMap<String, AllStyleDetails>();
        Collections.sort(list, new NetscoreComparatorForAllStyles());

        if (bumpAction == 1) {
            Collections.reverse(list);
            for (AllStyleDetails vo : list) {
                sortedMap.put(vo.getStyleid(), vo);
            }
        } else if (bumpAction == -1) {
            for (AllStyleDetails vo : list) {
                sortedMap.put(vo.getStyleid(), vo);
            }
        }

        return sortedMap;
    }

    public static Map<String, ElasticityRackVo> getStyleRackElasticity(Session session) {

        Map<String, ElasticityRackVo> map = new HashMap<String, ElasticityRackVo>();

        String sql = "select styleid, elasticity, articletype, gender, " +
                " cascelasticity_cd, cascelasticity_td, cascelasticity_unif, cascvisibility_elasticity " +
                " from tlp.rackelasticityinfo";

        ResultSet rs = session.execute(sql);

        if (rs != null) {
            Iterator<Row> iterator = rs.iterator();
            while (iterator.hasNext()) {
                Row row = iterator.next();
                ElasticityRackVo vo = new ElasticityRackVo();
                vo.setElasticity(row.getInt("elasticity"));
                vo.setCascElasticity_unif(row.getString("cascelasticity_unif"));
                vo.setCascElasticity_TD(row.getString("cascelasticity_td"));
                vo.setCascElasticity_CD(row.getString("cascelasticity_cd"));
                vo.setCascVisibility_Elasticity(row.getString("cascvisibility_elasticity"));

                FileElasticity.elasticCategorySet.add(row.getString("articletype").toLowerCase() + ":" + row.getString("gender").toLowerCase());
                map.put(row.getString("styleid"), vo);
            }
        }

        return map;
    }

    public static Map<String, StyleDetails> filterCategoryStyles(Map<String, StyleDetails> bigStyleDataMap, String brand, String article_type, String gender) {
        Map<String, StyleDetails> newMap = new HashMap<String, StyleDetails>();
        try {
            Connection connection = RedShiftConnectionFactory.getConnection();
            Statement statement = connection.createStatement();

            String liveStylesQuery = "select ps.style_id from pricing_snapshot ps" +
                    " JOIN dim_product dp on ps.style_id=dp.style_id" +
                    " where ps.is_live_on_portal=1 " +
                    " and dp.business_unit = 'Men''s Casual' " +
                    " and dp.brand_type = 'External' " +
                    " and dp.article_type = 'Jeans' " +
                    " and dp.gender = 'Men'" +
                    " and dp.season_code != 'FW17'" +
                    " and dp.commercial_type in ('OUTRIGHT', 'SOR') " +
                    " group by ps.style_id, ps.article_type, ps.brand, ps.gender " +
                    " order by ps.style_id ";

            java.sql.ResultSet liveStylesResult = statement.executeQuery(liveStylesQuery);
            Set<String> liveStylesSet = getLiveStylesSet(liveStylesResult);
            System.out.println("Size of set " + liveStylesSet.size());

            for (String styleId:liveStylesSet){
                if (bigStyleDataMap.containsKey(styleId)){
                    newMap.put(styleId, bigStyleDataMap.get(styleId));
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*try {

            BufferedReader br = new BufferedReader(new FileReader("/home/pricinguser/pricing_feed_data/" + brand.replaceAll("\\s+","").toLowerCase() + article_type.replaceAll("\\s+","").toLowerCase() + gender.toLowerCase() + ".csv"));
            String currentLine;
            try {
                while ((currentLine = br.readLine()) != null) {
                    String styleId = currentLine.split(",")[0];
                    if (bigStyleDataMap.containsKey(styleId)) {
                        newMap.put(styleId, bigStyleDataMap.get(styleId));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/

        return newMap;
    }

    public static Set<String> getLiveStylesSet(java.sql.ResultSet rs){
        HashSet<String> liveStylesSet = new HashSet<String>();
        try {
            while (rs.next()){
                liveStylesSet.add(rs.getString("style_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liveStylesSet;
    }

    public static ArrayList<BAG> readBagCSV(String filename){
        BufferedReader br = null;
        ArrayList<BAG> catList = new ArrayList<BAG>();
        try {
            br = new BufferedReader(new FileReader(new File(filename)));
            String line;
            line = br.readLine();
            while ((line=br.readLine()) != null){
                String []strings = line.split(",");
                BAG bag = new BAG(strings[0], strings[1], strings[2]);
                catList.add(bag);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return catList;
    }
}
