package com.myntra.main;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.myntra.util.Utility;
import com.myntra.vo.StyleDetails;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ComputedSnapshotReader {
	
	static String computedSnapshotSql = "select styleid, adjustedbmweightage, agefactor, articlemrp, articlemrpextax, articletype, " +
			" averageage, brand, brandtype, businessunit, buyingmargin, buyingmarginbackup, categorymanager, currentcommercialtype, " +
			" discount, discountfunding, discountid, discountlimit, discountname, discountrule, dohfactor, exclusion, " +
			" from_unixtime_expired_on, fundingpercentage, gender, getamount, getcount, getpercent, inventorycount, is_enabled, is_s20, " +
			" isactive, last30daysrevenue, last30dayssales, last7daysrevenue, last7dayssales, lastmodifiedon, liveonportal, " +
			" mastercategory, maxdiscount, mindiscount, mrpfactor, netscore, newbuyingmargin, norm_adj_bm, onbuyamount, onbuycount, " +
			" penaltyforhighdiscount, seasoncode, styleage, subcategory, supplytype, toi, totalscore, updatedby, updatedon " +
			" from aviator.computed_snapshot where date = %s and time = %s ";
	
	public static Map<String, Long> cSCategoryRatio = null;
	
	public static Map<String, StyleDetails> readComputedSnapshot(Session session, int currentDate, int currentTime) {

		//Adding logic to exclude styles where change count > 3 and orders count is greater than 1

		ResultSet rs1 = session.execute(String.format(computedSnapshotSql, currentDate, currentTime));

		Map<String, StyleDetails> styleComputedSnapshotDetailsMap = new HashMap<String, StyleDetails>();
		cSCategoryRatio = new HashMap<String, Long>();
		
		if (rs1 != null) {
			 Map<Integer, Double> styleDemandMap = DemandReader.readDemand(session);
			
			Iterator<Row> iterator = rs1.iterator();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				double last30dayssales = Utility.convertToDouble(row.getString("last30dayssales"));
				double last7dayssales = Utility.convertToDouble(row.getString("last7dayssales"));

				double dmdFinal = ((last7dayssales/7)+(last30dayssales/30))/2;

				StyleDetails styleDetails = new StyleDetails();
				styleDetails.setStyleid(row.getString("styleid"));
				styleDetails.setMinDiscount(Double.parseDouble(row.getString("mindiscount"))); 
				styleDetails.setMaxDiscount(Double.parseDouble(row.getString("maxdiscount")));
				styleDetails.setGetpercent(row.getString("getpercent"));
				styleDetails.setLast30dayssales(row.getString("last30dayssales"));
				styleDetails.setLast7dayssales(row.getString("last7dayssales"));


				if (styleDemandMap.containsKey(Integer.parseInt(row.getString("styleid")))) {
					styleDetails.setDmdfinal(styleDemandMap.get(Integer.parseInt(row.getString("styleid"))));
				} else {
					styleDetails.setDmdfinal(dmdFinal);
				}
				
				
				styleDetails.setArticlemrp(row.getString("articlemrp"));
				styleDetails.setArticletype(row.getString("articletype"));
				styleDetails.setNewbuyingmargin(Utility.convertToDouble(row.getString("newbuyingmargin")));
				styleDetails.setNorm_adj_bm(Utility.convertToDouble(row.getString("norm_adj_bm")));
				styleDetails.setGender(row.getString("gender"));
				styleDetails.setDiscountfunding(row.getString("discountfunding"));
				styleDetails.setFundingpercentage(row.getString("fundingpercentage"));
				styleDetails.setDiscountlimit(row.getString("discountlimit"));

				

				String fromUnixTimeExpiredOn = row.getString("from_unixtime_expired_on");
				
				if (fromUnixTimeExpiredOn.lastIndexOf('.') > 0) { 
					styleDetails.setFrom_unixtime_expired_on(fromUnixTimeExpiredOn.substring(0, fromUnixTimeExpiredOn.lastIndexOf('.')));
				} else {
					styleDetails.setFrom_unixtime_expired_on(row.getString("from_unixtime_expired_on"));
				}
				
				
				styleDetails.setGetcount(row.getString("getcount"));
				styleDetails.setGetamount(row.getString("getamount"));
				styleDetails.setBrand(row.getString("brand"));
				styleDetails.setBrandtype(row.getString("brandtype"));
				styleDetails.setCurrentcommercialtype(row.getString("currentcommercialtype"));
				styleDetails.setBusinessunit(row.getString("businessunit"));
				styleDetails.setDiscountid(row.getString("discountid"));
				styleDetails.setTotalscore(Utility.convertToDouble(row.getString("totalscore")));
				styleDetails.setNetscore(Utility.convertToDouble(row.getString("netscore")));
				styleDetails.setAverageage(row.getString("averageage"));
				styleDetails.setSeasoncode(row.getString("seasoncode"));
//				System.out.println(styleDetails.getSeasoncode());
				
				double normAdjBmNew = 0;

				if (styleDetails.getTotalscore() < styleDetails.getNewbuyingmargin()) {
					normAdjBmNew = styleDetails.getNewbuyingmargin();						
				} else {
					normAdjBmNew = (.75)*styleDetails.getNewbuyingmargin() + (.25)*styleDetails.getTotalscore();
				}

				styleDetails.setNorm_adj_bm_new_actual(normAdjBmNew);

				int factor = 0;

				if (normAdjBmNew%5 >= 2.5d) {
					factor = +5;
				} 

				double finalNormAdjBmNew = 5*(int)(normAdjBmNew/5) + factor;

				styleDetails.setNorm_adj_bm_new(finalNormAdjBmNew);
				styleComputedSnapshotDetailsMap.put(row.getString("styleid"), styleDetails);
			}
		}
		System.out.println("computed Snapshot size :" + styleComputedSnapshotDetailsMap.size());
		return styleComputedSnapshotDetailsMap;
	}	
}
