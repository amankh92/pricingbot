package com.myntra.main;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exclusion {

	static String countSql = "select style_id, count from tlp.style_change_count where date = %s";
	
	static String ordersSql = "select style from live_tracker.orders where date = %s";
	
	public static List<String> getStyles(Session session, int currentDate) {
		
		List<String> exclusionList = new ArrayList<String>();
		
		ResultSet countRs = session.execute(String.format(countSql, currentDate));
		
		if (countRs != null) {
			Iterator<Row> iterator = countRs.iterator();
			
			while (iterator.hasNext()) {
				Row row = iterator.next();
				int style = row.getInt("style_id");
				int count = row.getInt("count");

				if(count > 2) {
					exclusionList.add(String.valueOf(style));
				}
			}
		}
		
		System.out.println("exclusionList count : " + exclusionList.size());
		
		ResultSet ordersRs = session.execute(String.format(ordersSql, currentDate));
		
		if (ordersRs != null) {
			Iterator<Row> iterator = ordersRs.iterator();
			
			while (iterator.hasNext()) {
				Row row = iterator.next();
				long style = row.getLong("style");
				exclusionList.add(String.valueOf(style));
			}
		}
		
		System.out.println("exclusionList count : " + exclusionList.size());
		
		return exclusionList;
	}

}
