package com.myntra.main;

import com.myntra.vo.StyleDetails;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FileGenerator {

	public static List<File> generateFile(int bumpAction, int currentDate, int currentTime, Map<String, StyleDetails> dataMap) {

		String header = "styleId, currentTd, newTd, minDiscount, maxDiscount, norm_adj_bm_new, totalscore, dmdFinal, " +
				"articlemrp, articletype, gender, discountFunding, fundingPercentage, discountLimit, " +
				"from_unixtime_expired_on, getCount, getAmount, Brand, discountId, Brandtype, " +
				"Businessunit, Currentcommercialtype, AverageAge, health, normAdjBmBreach, netscore, elasticity_band";

		List<File> fileList = new ArrayList<File>();

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {

			if (bumpAction == 1) {
				file = new File("./files/sent_bumpRevenue_"+currentDate+""+currentTime + ".csv");
			} else if (bumpAction == -1) {
				file = new File("./files/sent_bumpMargin_"+currentDate+""+currentTime + ".csv");
			}
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for(Map.Entry<String, StyleDetails> entry : dataMap.entrySet()) {
				StyleDetails val = entry.getValue();
				if (val!= null ) {
					bufferedWriter.write("\n");
					bufferedWriter.write(val.getStyleid() +", " + val.getCurrentTd()+", " + val.getNewTd()
							+", " + val.getMinDiscount()+", " + val.getMaxDiscount()+", " + val.getNorm_adj_bm_new_actual()+", " + val.getTotalscore()
							+", " + val.getDmdfinal()+", " + val.getArticlemrp()+", " + val.getArticletype()
							+", " + val.getGender()+", " + val.getDiscountfunding()
							+", " + val.getFundingpercentage()+", " + val.getDiscountlimit()
							+", " + val.getFrom_unixtime_expired_on()+", " + val.getGetcount()
							+", " + val.getGetamount()+", " + val.getBrand()+", " + val.getDiscountid()
							+", " + val.getBrandtype()+", " + val.getBusinessunit()+", " + val.getCurrentcommercialtype()
							+", " + val.getAverageage()
							+", " + val.getHealth()+", " + val.getNorm_adj_bm_breach()
							+", " + val.getNetscore()+", " + val.getElasticityBand()
							);
				} else {
					System.out.println("Data not found for style : " + val.getStyleid());
				}
			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			return null;
		} 
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			} 
		}
		fileList.add(file);
		return fileList;
	}

	public static List<File> generateFile(int bumpAction, int currentDate, int currentTime, Map<String, StyleDetails> dataMap, String article_type, String gender) {

		String header = "styleId, currentTd, newTd, minDiscount, maxDiscount, norm_adj_bm_new, totalscore, dmdFinal, " +
				"articlemrp, articletype, gender, discountFunding, fundingPercentage, discountLimit, " +
				"from_unixtime_expired_on, getCount, getAmount, Brand, discountId, Brandtype, " +
				"Businessunit, Currentcommercialtype, AverageAge, health, normAdjBmBreach, netscore, elasticity_band";

		List<File> fileList = new ArrayList<File>();

		File file = null;
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		try {

			if (bumpAction == 1) {
				file = new File("./files/sent_bumpRevenue_" + article_type + gender + currentDate+"" + currentTime + ".csv");
			} else if (bumpAction == -1) {
				file = new File("./files/sent_bumpMargin_" + article_type + gender +currentDate+"" + currentTime + ".csv");
			}
			fileWriter = new FileWriter(file);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(header);

			for(Map.Entry<String, StyleDetails> entry : dataMap.entrySet()) {
				StyleDetails val = entry.getValue();
				if (val!= null ) {
					bufferedWriter.write("\n");
					bufferedWriter.write(val.getStyleid() +", " + val.getCurrentTd()+", " + val.getNewTd()
							+", " + val.getMinDiscount()+", " + val.getMaxDiscount()+", " + val.getNorm_adj_bm_new_actual()+", " + val.getTotalscore()
							+", " + val.getDmdfinal()+", " + val.getArticlemrp()+", " + val.getArticletype()
							+", " + val.getGender()+", " + val.getDiscountfunding()
							+", " + val.getFundingpercentage()+", " + val.getDiscountlimit()
							+", " + val.getFrom_unixtime_expired_on()+", " + val.getGetcount()
							+", " + val.getGetamount()+", " + val.getBrand()+", " + val.getDiscountid()
							+", " + val.getBrandtype()+", " + val.getBusinessunit()+", " + val.getCurrentcommercialtype()
							+", " + val.getAverageage()
							+", " + val.getHealth()+", " + val.getNorm_adj_bm_breach()
							+", " + val.getNetscore()+", " + val.getElasticityBand()
					);
				} else {
					System.out.println("Data not found for style : " + val.getStyleid());
				}
			}
		} catch (IOException e) {
			System.out.println("Exception raised while writing to report : " + e);
			e.printStackTrace();
			return null;
		}
		finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Exception raised while closing writer objects : "+ e);
				e.printStackTrace();
			}
		}
		fileList.add(file);
		return fileList;
	}
}
