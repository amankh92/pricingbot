package com.myntra.main;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.myntra.vo.ElasticityRackVo;

public class RackReader {
	
	static String sql = "select styleid, articleType, gender, collection, rack, elasticity, " +
				" cascelasticity_cd, cascelasticity_td, cascelasticity_unif, cascvisibility_elasticity " +
			" from tlp.rackelasticityinfo ";
	
	public static Map<String, ElasticityRackVo> readRackInfo(Session session) {

		ResultSet rs1 = session.execute(sql);
		Map<String, ElasticityRackVo> map = new HashMap<String, ElasticityRackVo>();
		
		if (rs1 != null) {
			Iterator<Row> iterator = rs1.iterator();
			while (iterator.hasNext()) {
				Row row = iterator.next();
				ElasticityRackVo vo = new ElasticityRackVo();
				vo.setStyleId(row.getString("styleid"));
				vo.setArticleType(row.getString("articleType"));
				vo.setGender(row.getString("gender"));
				vo.setElasticity(row.getInt("elasticity"));

				if (vo.getElasticity() == 0) {
					vo.setElasticityType("0");
				} else if (vo.getElasticity() == 1 || vo.getElasticity() == 2) {
					vo.setElasticityType("1,2");
				} else if (vo.getElasticity() == 3 || vo.getElasticity() == 4) {
					vo.setElasticityType("3,4");
				}

				vo.setRack(row.getString("rack"));
				vo.setCollection(row.getString("collection"));
				vo.setCascElasticity_CD(row.getString("cascelasticity_cd"));
				vo.setCascElasticity_TD(row.getString("cascelasticity_td"));
				vo.setCascElasticity_unif(row.getString("cascelasticity_unif"));
				vo.setCascVisibility_Elasticity(row.getString("cascvisibility_elasticity"));
				map.put(vo.getStyleId(), vo);
			}
		}

		System.out.println("RackMap size : " + map.size());
		return map;
	}	
}
