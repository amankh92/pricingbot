package com.myntra.main;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DemandReader {
	
	static String demandSql = "select style_id, demand " +
			" from tlp.style_demand ";
	
	public static Map<Integer, Double> styleDemandMap = null;
	
	public static Map<Integer, Double> readDemand(Session session) {

		if (styleDemandMap == null) {
			styleDemandMap = new HashMap<Integer, Double>();
			
			ResultSet rs1 = session.execute(demandSql);
	
			if (rs1 != null ) {
				Iterator<Row> iterator = rs1.iterator();
				while (iterator.hasNext()) {
					Row row = iterator.next();
					int style_id = row.getInt("style_id");
					double demand = row.getDouble("demand"); 
					styleDemandMap.put(style_id, demand);
				}
			}
		}
		System.out.println("StyleDemandMap size :" + styleDemandMap.size());
		
		return styleDemandMap;
	}	
}
