package com.myntra.main;

import com.myntra.vo.StyleDetails;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;


public class DiscountTracker {
    public static void trackDiscounts(Map<String, StyleDetails> revMap, Map<String, StyleDetails> marginMap, Map<String, StyleDetails> computedSnapshotMap, int currentDate, int currentTime) {
        String header = "styleId, currentTD, newTD";

        File file = new File("./discount_tracker/moda_rapido_discounts_" + currentDate + "_" + currentTime);
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            fileWriter = new FileWriter(file);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(header);

            for (String str : computedSnapshotMap.keySet()){
                if (revMap.containsKey(str)){
                    bufferedWriter.write("\n");
                    bufferedWriter.write(str + ", " + revMap.get(str).getNewTd());
                }
                else if (marginMap.containsKey(str)){
                    bufferedWriter.write("\n");
                    bufferedWriter.write(str + ", " + marginMap.get(str).getNewTd());
                }
                else {
                    bufferedWriter.write("\n");
                    bufferedWriter.write(str + ", " + computedSnapshotMap.get(str).getGetpercent());
                }
            }
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
