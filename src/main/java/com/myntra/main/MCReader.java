package com.myntra.main;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.myntra.vo.AllStyleDetails;
import com.myntra.vo.CategoryStyleRow;
import com.myntra.vo.StyleDetails;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MCReader {
	
	static String calculateDiscountPercentSql = "select style_id, category, bm_postprov, discount_td_band, dmdfinal, td, td_new, " +
			" td_new_strong, td_new_weak " +
			" from tlp.category_style_mapping " +
//			" from tlp.category_style_mapping_new " +
			" where category in " +
			" ('Tshirts_Men','Tops_Women','Sports#Shoes_Men','Casual#Shoes_Men','Jeans_Men','Jeans_Women','Shirts_Men'," +
			" 'Shirts_Women','Trousers_Women','Trousers_Men','Dresses_Women','Tshirts_Women','Formal#Shoes_Men','Heels_Women'," +
			" 'Kurtas_Women','Flats_Women','Jackets_Men','Watches_Women','Handbags_Women','Watches_Men')";

	public static Map<String, AllStyleDetails> readMC(Session session, Map<Integer, Double> elasticityDiscount, Map<String, StyleDetails> computedSnapShotMap, boolean highDemand, boolean firstCall) {

		ResultSet rs = session.execute(calculateDiscountPercentSql);

		Map<String, CategoryStyleRow> selectedStyleElasticityMap = new HashMap<String, CategoryStyleRow>();

		int mcTotalCount = 0;
		
		if (rs != null ) {
			Iterator<Row> iterator = rs.iterator();
			while (iterator.hasNext()) {
				mcTotalCount++;
				Row row = iterator.next();
				if (elasticityDiscount.containsKey(row.getInt("discount_td_band"))) {
					CategoryStyleRow categoryStyleRow = new CategoryStyleRow();
					categoryStyleRow.setbM_postprov(row.getDouble("bm_postprov"));
					categoryStyleRow.setStyle_id(row.getString("style_id"));
					categoryStyleRow.setDiscountTDBand(row.getInt("discount_td_band"));
					categoryStyleRow.setTd(row.getDouble("td"));
					categoryStyleRow.setTd_new(row.getDouble("td_new"));
					categoryStyleRow.setCategory(row.getString("category").replace('_', ':').replace('#', ' '));
					selectedStyleElasticityMap.put(row.getString("style_id"), categoryStyleRow);
				}
			}
		}
		
		System.out.println("MC total count :  " + mcTotalCount + "    MCSelectedElasticityCount :  " + selectedStyleElasticityMap.size());
		
		Map<String, AllStyleDetails> selectedStyleMap = new HashMap<String, AllStyleDetails>();

		int j = 0;

		int computedSnapShotMapNull = 0;
		int getPercentNull = 0;
		
		if (selectedStyleElasticityMap.size() > 0) {

			for (Map.Entry<String, CategoryStyleRow> entry : selectedStyleElasticityMap.entrySet()) {
				
				CategoryStyleRow categoryStyleRow = entry.getValue();
				
				if (computedSnapShotMap.get(entry.getKey().trim()) != null && computedSnapShotMap.get(entry.getKey().trim()).getGetpercent() != null) {
				
					AllStyleDetails st = new AllStyleDetails();
					double curr_dis = Double.parseDouble(computedSnapShotMap.get(entry.getKey()).getGetpercent()); 

					st.setStyleid(entry.getKey());
					st.setCurrentdiscount(curr_dis);
					st.setNewdiscount(curr_dis + elasticityDiscount.get(categoryStyleRow.getDiscountTDBand()));

					if (firstCall) {
						st.setNewdiscount(categoryStyleRow.getTd_new());
					}

					st.setDmdFinal(computedSnapShotMap.get(entry.getKey()).getDmdfinal());
					st.setElasticityBand(categoryStyleRow.getDiscountTDBand());

					st.setCategory(categoryStyleRow.getCategory());

					if (st.getNewdiscount() > 0) {
						if (st.getNewdiscount() <= computedSnapShotMap.get(entry.getKey()).getNorm_adj_bm_new()) {
							selectedStyleMap.put(entry.getKey(), st);
						} else {
							st.setNorm_adj_bm_breach(1);
							st.setNewdiscount(computedSnapShotMap.get(entry.getKey()).getNorm_adj_bm_new());
							selectedStyleMap.put(entry.getKey(), st);
						}
					} else {
						j++;
					}
				} else if (computedSnapShotMap.get(entry.getKey().trim()) == null ) {
					//csNotfoundStyles += entry.getKey()+",";
					computedSnapShotMapNull++;
				} else if (computedSnapShotMap.get(entry.getKey().trim()) != null  && computedSnapShotMap.get(entry.getKey().trim()).getGetpercent() == null) {
					getPercentNull++;
				}

			}

		}

		
		System.out.println(" MC count   :   " + selectedStyleMap.size() + "     0 newTd count   :   " + j);
		System.out.println(" computedSnapShotMapNull   :   " + computedSnapShotMapNull + "   getPercentNull   :   " + getPercentNull);
		return selectedStyleMap;
	}

}
