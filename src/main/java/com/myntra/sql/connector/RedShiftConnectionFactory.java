package com.myntra.sql.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class RedShiftConnectionFactory {

	private static Logger logger = LoggerFactory.getLogger(RedShiftConnectionFactory.class);

	private static RedShiftConnectionFactory instance = new RedShiftConnectionFactory();
	static final String dbURL = "jdbc:redshift://dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw"; 
	static final String MasterUsername = "datasciences_ro";
	static final String MasterUserPassword = "DaTaScIences@123ro";

	static Connection connection = null;
	
	private RedShiftConnectionFactory() {

		try {
			Class.forName("com.amazon.redshift.jdbc4.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("Exception while loading driver : ", e);
		}

	}

	private static Connection createConnection() {

		try {
			//Open a connection and define properties.
			System.out.println("Connecting to Redshift database...");
			logger.debug("Connecting to Redshift database...");

			Properties props = new Properties();

			//Uncomment the following line if using a keystore.
			//props.setProperty("ssl", "true");  
			props.setProperty("user", MasterUsername);
			props.setProperty("password", MasterUserPassword);
			connection = DriverManager.getConnection(dbURL, props);

			System.out.println("Connection with database established successfully");
			logger.debug("Connection with Redshift database established successfully");


		} catch (SQLException e) {
			logger.error("Unable to connect to Database ", e);
			System.out.println("ERROR: Unable to Connect to Database.  " + e);
		}
		return connection;
	}   

	public static Connection getConnection() {
		if (connection == null) {
			if (instance == null) {
				instance = new RedShiftConnectionFactory();
			}
			return instance.createConnection();
		}
		return connection;
	}
	
	public static void close() {
		System.out.println("Closing connection with redshift database...");
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			instance = null;
		}
	}

}
