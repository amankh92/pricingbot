package com.myntra.sql.connector;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class CassandraConnector {
	private Cluster cluster;
	private Session session;

	public void connect(final String[] node, final int port)
	{
		System.out.println("Connecting to Cassandra DB...");
		this.cluster = Cluster.builder().addContactPoints(node).withPort(port).build();
		session = cluster.connect();
	}

	public Session getSession()
	{
		return this.session;
	}
	
	public Session getSession(final String[] node, final int port)
	{
		if (this.session == null) {
			connect(node, port);
		}
		return this.session;
	}
	
	public void close()
	{
		System.out.println("Closing Cassandra connection...");
		cluster.close();
	}
}
