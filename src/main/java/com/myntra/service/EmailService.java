package com.myntra.service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.util.List;
import java.util.Properties;


public class EmailService {

    public void sendEmail(List<File> file1, List<File> file2, int currentDate, int currentTime, int actualCurrentTime, String brand, String article_type, String gender, String[] recipients) {

        final String username = "revenue-labs-ds@myntra.com";
        final String password = "myntracom";


        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username,password);
                    }
                });

        try {

//			String[] recipients = {"sorabh.kumar@myntra.com","prasad.joshi@myntra.com","kunal.singh@myntra.com","deepak.warrier@myntra.com","bhavesh.singhal@myntra.com","apoorv.kalra@myntra.com","kaivalya.kumar@myntra.com","aman.khandelwal@myntra.com","aditya.singh@myntra.com","vinothkumar.perumalsamy@myntra.com","pricing@myntra.com","ashish.rawat@myntra.com"};
//            String[] recipients = {"aman.khandelwal@myntra.com"};

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++)
            {
                addressTo[i] = new InternetAddress(recipients[i]);
            }

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sorabh.kumar@myntra.com"));
            message.setRecipients(Message.RecipientType.TO, addressTo);
            message.setSubject("TLP discounts for " + brand + " " + article_type.toUpperCase() + " " + gender.toUpperCase() + " for " + currentDate +":" + actualCurrentTime);

            Multipart multipart = new MimeMultipart();

            for (File f : file1) {
                BodyPart messageBodyPart1 = new MimeBodyPart();
                try {
                    messageBodyPart1.setDisposition(Message.ATTACHMENT);

                    DataSource source = new FileDataSource(f);
                    messageBodyPart1.setDataHandler(new DataHandler(source));
                    messageBodyPart1.setFileName(f.getName());

                    multipart.addBodyPart(messageBodyPart1);
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            for (File f : file2) {
                BodyPart messageBodyPart = new MimeBodyPart();
                try {
                    messageBodyPart.setDisposition(Message.ATTACHMENT);

                    DataSource source = new FileDataSource(f);
                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(f.getName());

                    multipart.addBodyPart(messageBodyPart);
                } catch (MessagingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDisposition(Message.INLINE);
            messageBodyPart.setContent(createMessageBody(currentTime, actualCurrentTime), "text/html; charset=\"utf-8\"");
            multipart.addBodyPart(messageBodyPart);

            message.setContent(multipart);
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private String createMessageBody(int currentTime, int actualCurrentTime) {

        StringBuilder messageBody = new StringBuilder();

        messageBody.append("<div>PFA style level discounts (small/large) to bump revenue/margin.</div><br>");

        messageBody.append("<div>This is an automatically generated email – please do not reply to it.</div></br>");

        return messageBody.toString();
    }

    private String[] readEmailIds() {
        // TODO Auto-generated method stub

        File file_1 = new File("/home/pricinguser/DailyMetricTracking/validate_csvdir/emailIds.txt");
        //File file_1 = new File("/home/en-sorabhk/Documents/emailIds.txt");
        String[] strArr = null;

        BufferedReader in_1 = null;
        try {
            in_1 = new BufferedReader(new FileReader(file_1));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String str_1;
        try {

            while ((str_1 = in_1.readLine()) != null) {
                strArr = str_1.split(",");
            }
            in_1.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

		/*for (String s: strArr) {
			System.out.println(s);
		}*/
        return strArr;

    }

	/*public static void main(String[] args) {
		EmailSender emailSender = new EmailSender();
		emailSender.readEmailIds();
	}*/
}
