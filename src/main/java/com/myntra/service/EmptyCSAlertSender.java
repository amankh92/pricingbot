package com.myntra.service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;


public class EmptyCSAlertSender {

	public static void sendEmail(int currentDate, int currentTime) {

		final String username = "revenue-labs-ds@myntra.com";
		final String password = "myntracom";
		

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username,password);
			}
		});

		try {
			String[] recipients = {"aman.khandelwal@myntra.com"};

			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++)
			{
				addressTo[i] = new InternetAddress(recipients[i]);
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("sorabh.kumar@myntra.com"));
			message.setRecipients(Message.RecipientType.TO, addressTo);
			message.setSubject("ComputedSnapshot Empty for "+ currentDate +":" + currentTime);

			Multipart multipart = new MimeMultipart();

			BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setDisposition(Message.INLINE);
            messageBodyPart.setContent(createMessageBody(currentDate, currentTime), "text/html; charset=\"utf-8\"");
            multipart.addBodyPart(messageBodyPart);
			
			message.setContent(multipart);
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	private static String createMessageBody(int currentDate, int currentTime) {

		StringBuilder messageBody = new StringBuilder();

		messageBody.append("<div>Alert!!..  ComputedSnapshot is empty for "+currentDate+":"+currentTime+"</div><br>");

		messageBody.append("<div>This is an automatically generated email – please do not reply to it.</div></br>");

		return messageBody.toString();
	}
}
