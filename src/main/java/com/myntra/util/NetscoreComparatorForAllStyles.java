package com.myntra.util;

import com.myntra.vo.AllStyleDetails;

import java.util.Comparator;

public class NetscoreComparatorForAllStyles implements Comparator<AllStyleDetails>{

	public int compare(AllStyleDetails o1, AllStyleDetails o2) {
		// TODO Auto-generated method stub
		 if ((o1.getNetScore() - o2.getNetScore()) < .000000001)
		        return 0;
		    if (o1.getNetScore() < o2.getNetScore())
		        return -1;
		    else if (o1.getNetScore() > o2.getNetScore())
		        return 1;
		    return 0;
	}

}
