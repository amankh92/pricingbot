package com.myntra.util;

import com.myntra.vo.ElasticityRackVo;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FileElasticity {
	
	public static Map<String, ElasticityRackVo> fileStyleElasticity = new HashMap<String, ElasticityRackVo>();
	public static Set<String> elasticCategorySet = new HashSet<String>();
	
	public static void calculateFileElasticity() {
		//File fileL = new File("/home/en-sorabhk/drive/");
		File fileL = new File(".");
		System.out.println("Current Location : " + fileL.getAbsolutePath());
		
		//File file_1 = new File("/home/en-sorabhk/drive/elasticity_dir/Jan_style_elasticity.csv");
		File file_1 = new File("./Jan_style_elasticity.csv");
		
		BufferedReader in_1 = null;
		try {
			in_1 = new BufferedReader(new FileReader(file_1));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str_1;
	    try {
			String[] strArr = null;
			while ((str_1 = in_1.readLine()) != null) {
			    strArr = str_1.split(",");
			    if (Utility.isInt(strArr[10])) {
			    	ElasticityRackVo vo = new ElasticityRackVo();
			    	vo.setElasticity(Integer.parseInt(strArr[10]));
					vo.setCascElasticity_unif(strArr[6]);
					vo.setCascElasticity_TD(strArr[7]);
					vo.setCascElasticity_CD(strArr[8]);
					vo.setCascVisibility_Elasticity(strArr[9]);
					elasticCategorySet.add(strArr[1].replace("\"", "").toLowerCase()+":"+strArr[2].replace("\"", "").toLowerCase());
			    	fileStyleElasticity.put(strArr[0], vo);
			    }
			}
			in_1.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	    
	    /*for (Map.Entry<String, Long> entry : fileStyleElasticity.entrySet()) {
	    	System.out.println(entry.getKey() + "    *******  " + entry.getValue());
	    }
	    System.out.println(fileStyleElasticity.size());*/
	    
	
	    /*
		File file = new File("/home/en-sorabhk/drive/elasticity_dir/Jan_Rack_elasticity.csv");
		
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String str;
	    try {
			String[] strArr = null;
			while ((str = in.readLine()) != null) {
			    strArr = str.split(",");
			    staticMapping.put((strArr[1]+"|"+strArr[2]+"|"+strArr[3]+"|"+strArr[4]).replace("\"", "").toLowerCase(), strArr[9]);
			    if (!strArr[1].equals("\"article_type\"")) {
			    	CollectionElasticityTdMappingFileInfo vo = new CollectionElasticityTdMappingFileInfo();
			    	vo.setArticle_type(strArr[1]);
			    	vo.setGender(strArr[2]);
			    	vo.setCollecName(strArr[3]);
			    	vo.setRack_id(strArr[4]);
			    	
			    	vo.setCascElasticity_unif(Utility.isDouble(strArr[5])?Double.parseDouble(strArr[5]):1);
				    vo.setCascElasticity_TD(Utility.isDouble(strArr[6])?Double.parseDouble(strArr[6]):1);
				    vo.setCascElasticity_CD(Utility.isDouble(strArr[7])?Double.parseDouble(strArr[7]):1);
				    vo.setCascVisibility_Elasticity(Utility.isDouble(strArr[8])?Double.parseDouble(strArr[8]):1);
				    
				    vo.setDiscountTDBand(Utility.isInt(strArr[9])?Integer.parseInt(strArr[9]):0);
			    	staticMappingAll.put((strArr[1]+"|"+strArr[2]+"|"+strArr[3]+"|"+strArr[4]).replace("\"", "").toLowerCase(), vo);
			    }
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} */
	}
}
