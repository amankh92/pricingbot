package com.myntra.util;

import com.myntra.vo.AllStyleDetails;

import java.util.Comparator;

public class AllStyleDetailsComparator implements Comparator<AllStyleDetails>{

	public int compare(AllStyleDetails o1, AllStyleDetails o2) {
		// TODO Auto-generated method stub
		if (o1.getDmdFinal() < o2.getDmdFinal()) return -1;
        if (o1.getDmdFinal() > o2.getDmdFinal()) return 1;
        return 0;
	}

}
