package com.myntra.util;

import com.myntra.vo.StyleDetails;

import java.util.Comparator;

public class NetscoreComparatorForStyleDetails implements Comparator<StyleDetails>{

	public int compare(StyleDetails o1, StyleDetails o2) {
		// TODO Auto-generated method stub
//		 if ((o1.getNetscore() - o2.getNetscore()) < .000000001)
//		        return 0;
//		    if (o1.getNetscore() < o2.getNetscore())
//		        return -1;
//		    else if (o1.getNetscore() > o2.getNetscore())
//		        return 1;
//		    return 0;
		if (o1.getNetscore() < o2.getNetscore()) return -1;
		if (o1.getNetscore() > o2.getNetscore()) return 1;
		return 0;
	}

}
