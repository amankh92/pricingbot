package com.myntra.util;

import com.myntra.vo.NetScoreVO;

import java.util.Comparator;

public class NetscoreComparator implements Comparator<NetScoreVO> {

	public int compare(NetScoreVO o1, NetScoreVO o2) {
		// TODO Auto-generated method stub
		/*if (o1.getNetScore() < o2.getNetScore()) {
			return -1;
		} else if (o1.getNetScore() > o2.getNetScore()) {
			return 1;
		} else {
			return 0;
		}*/
		 if ((o1.getNetScore() - o2.getNetScore()) < .000000001)
		        return 0;
		    if (o1.getNetScore() < o2.getNetScore())
		        return -1;
		    else if (o1.getNetScore() > o2.getNetScore())
		        return 1;
		    return 0;
		//return o1.getNetScore()==o2.getNetScore()?0:(o1.getNetScore()<o2.getNetScore()?-1:1);
	}
	
}
