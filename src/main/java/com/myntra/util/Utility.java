package com.myntra.util;

import com.myntra.vo.AllStyleDetails;
import com.myntra.vo.StyleDetails;

import java.util.*;

public class Utility {

	public static double checkForNan(double num) {

		if (((Double)num).isNaN() || ((Double)num).isInfinite()) {
			return 0;
		}
		return num;
	}

	public static double convertToDouble(String num) {

		if (num != null && !num.trim().equals("")) {
			try
			{
				return Double.parseDouble(num);
			}
			catch(NumberFormatException e)
			{
				System.out.println("error while parsing string "+ num + " to double ");
				return 0;
			}
		}
		return 0;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}

	public static boolean isDouble(String num) {

		if (num != null && !num.trim().equals("")) {
			try
			{
				Double.parseDouble(num);
				return true;
			}
			catch(NumberFormatException e)
			{
				return false;
			}
		}
		return false;
	}

	public static boolean isInt(String num) {

		try
		{
			Integer.parseInt(num);
		}
		catch(NumberFormatException e)
		{
			return false;
		}
		return true;

	}

	public static Map<String, AllStyleDetails> sortByDmdfinal(Map<String, AllStyleDetails> map) {

		List<AllStyleDetails> list = new ArrayList<AllStyleDetails>();

		for(Map.Entry<String, AllStyleDetails> entry : map.entrySet()) {
			list.add(entry.getValue());
		}

		Collections.sort(list, new DmdFinalComparator());
		Collections.reverse(list);

		Map<String, AllStyleDetails> sortedHashMap = new LinkedHashMap<String, AllStyleDetails>();

		for (AllStyleDetails vo : list) {
			sortedHashMap.put(vo.getStyleid(), vo);
		}
		return sortedHashMap;
	}
	
	public static Map<String, StyleDetails> sortStyleDetailsByDmdfinal(Map<String, StyleDetails> map) {

		List<StyleDetails> list = new ArrayList<StyleDetails>();

		for(Map.Entry<String, StyleDetails> entry : map.entrySet()) {
			list.add(entry.getValue());
		}

		Collections.sort(list, new DmdFinalComparatorForStyleDetails());
		Collections.reverse(list);

		Map<String, StyleDetails> sortedHashMap = new LinkedHashMap<String, StyleDetails>();

		for (StyleDetails vo : list) {
			sortedHashMap.put(vo.getStyleid(), vo);
		}
		return sortedHashMap;
	}

	public static Map<String, StyleDetails> sortMap(Map<String, StyleDetails> map, int bumpAction) {
		
		List<StyleDetails> list = new ArrayList<StyleDetails>();
		
		for(Map.Entry<String, StyleDetails> entry : map.entrySet()) {
			list.add(entry.getValue());
		}
		
		Collections.sort(list, new NetscoreComparatorForStyleDetails());
//		Collections.sort(list, new NormAdjBMNewComparator());
		
		if (bumpAction == 1) {
			Collections.reverse(list);
		} 
		
		Map<String, StyleDetails> sortedHashMap = new LinkedHashMap<String, StyleDetails>();

		for (StyleDetails vo : list) {
			sortedHashMap.put(vo.getStyleid(), vo);
		}
		/*for (String str : sortedHashMap.keySet()){
            System.out.println(sortedHashMap.get(str).getNetscore());
        }*/
		return sortedHashMap;
	}
	
	public static Map<String, AllStyleDetails> sortAllStyleDetailsMap(Map<String, AllStyleDetails> map) {
		
		List<AllStyleDetails> list = new ArrayList<AllStyleDetails>();
		
		for(Map.Entry<String, AllStyleDetails> entry : map.entrySet()) {
			list.add(entry.getValue());
		}
		
		Collections.sort(list, new DmdFinalComparator());
		Collections.reverse(list);
		
		Map<String, AllStyleDetails> sortedHashMap = new LinkedHashMap<String, AllStyleDetails>();

		for (AllStyleDetails vo : list) {
			sortedHashMap.put(vo.getStyleid(), vo);
		}
		
		return sortedHashMap;
	}

	public static Map<String, StyleDetails> final15Check(int i, Map<String, StyleDetails> map) {
		// TODO Auto-generated method stub
		
		for(Map.Entry<String, StyleDetails> entry : map.entrySet()) {
			StyleDetails vo = entry.getValue();
			if(i==1 && (vo.getNewTd()-vo.getCurrentTd()>15)) {
				
				double newTd = vo.getCurrentTd()+15;
				if ((newTd <= vo.getMaxDiscount()) && (newTd >= vo.getMinDiscount())) {
					vo.setNewTd(newTd);
				} else {
					if (newTd < vo.getMinDiscount()) {
						vo.setNewTd(vo.getMinDiscount());
					}
					if (newTd > vo.getMaxDiscount()) {
						vo.setNewTd(vo.getMaxDiscount());
					}
				}
				
				map.put(entry.getKey(), vo);
			} else if(i==-1 && (vo.getCurrentTd()-vo.getNewTd()>15)) {
				
				double newTd = vo.getCurrentTd()-15;
				if ((newTd <= vo.getMaxDiscount()) && (newTd >= vo.getMinDiscount())) {
					vo.setNewTd(newTd);
				} else {
					if (newTd < vo.getMinDiscount()) {
						vo.setNewTd(vo.getMinDiscount());
					}
					if (newTd > vo.getMaxDiscount()) {
						vo.setNewTd(vo.getMaxDiscount());
					}
				}
				
				map.put(entry.getKey(), vo);
			}
		}
		
		return map;
	}

}
