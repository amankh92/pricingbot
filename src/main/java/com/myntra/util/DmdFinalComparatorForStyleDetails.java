package com.myntra.util;

import com.myntra.vo.StyleDetails;

import java.util.Comparator;

public class DmdFinalComparatorForStyleDetails implements Comparator<StyleDetails>{

	/*public int compare(StyleDetails o1, StyleDetails o2) {

		double val1 = Utility.round(o1.getDmdfinal(), 5);
		double val2 = Utility.round(o2.getDmdfinal(), 5);

		    if (val1 < val2)
		        return -1;
		    else if (val1 > val2)
		        return 1;
		    return 0;
	}*/

	public int compare(StyleDetails o1, StyleDetails o2) {
		// TODO Auto-generated method stub

		if (o1.getDmdfinal() < o2.getDmdfinal()) {
			return -1;
		} else if (o1.getDmdfinal() > o2.getDmdfinal()) {
			return 1;
		}
		return 0;
	}

}
