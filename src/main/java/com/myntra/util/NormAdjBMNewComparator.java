package com.myntra.util;

import com.myntra.vo.StyleDetails;

import java.util.Comparator;

public class NormAdjBMNewComparator implements Comparator<StyleDetails> {
	public int compare(StyleDetails o1, StyleDetails o2){
		if (o1.getNorm_adj_bm_new_actual() > o2.getNorm_adj_bm_new_actual()){
			return 1;
		}
		else if (o1.getNorm_adj_bm_new_actual() < o2.getNorm_adj_bm_new_actual()){
			return -1;
		}
		return 0;
	}
}
