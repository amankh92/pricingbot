package com.myntra.util;

import java.util.Comparator;
import com.myntra.vo.StyleDemand;

public class StyleDemandComparator implements Comparator<StyleDemand>{

	/*public int compare(StyleDemand o1, StyleDemand o2) {
		// TODO Auto-generated method stub
		if (o1.getDemand() < o2.getDemand()) return -1;
        if (o1.getDemand() > o2.getDemand()) return 1;
        return 0;
	}*/

    public int compare(StyleDemand o1, StyleDemand o2) {
        // TODO Auto-generated method stub
        if (o1.getVisibilityDemand() < o2.getVisibilityDemand()) return -1;
        if (o1.getVisibilityDemand() > o2.getVisibilityDemand()) return 1;
        return 0;
    }
}
